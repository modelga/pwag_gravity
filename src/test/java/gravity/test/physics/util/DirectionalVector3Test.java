package gravity.test.physics.util;

import static gravity.test.assertion.AmountAssertion.assertAmount;
import static org.jscience.physics.amount.Amount.valueOf;
import gravity.physics.util.ForcedVector3;
import gravity.physics.util.Position;
import gravity.physics.util.Vector3;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Force;
import javax.measure.quantity.Length;
import javax.measure.quantity.Power;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test()
public class DirectionalVector3Test {

	public void forceShouldBeZero() {
		// given
		Amount<Length> zero = Amount.valueOf(0, SI.METER);
		Position joint = new Position(zero, zero, zero);

		Amount<Force> quantity = Amount.valueOf(1, SI.NEWTON);

		ForcedVector3<Force> vecPlus = new ForcedVector3<Force>(joint, getDirection(Amount.ONE), quantity);
		ForcedVector3<Force> vecMinus = new ForcedVector3<Force>(joint, getDirection(Amount.ONE.opposite()), quantity);

		// when
		ForcedVector3<Force> result = vecPlus.add(vecMinus);

		// then
		assertAmount(result.getPower()).approximates(valueOf(0, SI.NEWTON));
	}

	// public void forceShouldBeAdd() {
	// // given
	// Amount<Length> zero = Amount.valueOf(0, SI.METER);
	// Position joint = new Position(zero, zero, zero);
	//
	// Amount<Force> quantity = Amount.valueOf(1, SI.NEWTON);
	//
	// ForcedVector3<Force> vecPlus = new ForcedVector3<Force>(joint,
	// getDirection(Amount.ONE), quantity);
	// ForcedVector3<Force> vecMinus = new ForcedVector3<Force>(joint,
	// getDirection(Amount.ONE), quantity);
	//
	// // when
	// ForcedVector3<Force> result = vecPlus.add(vecMinus);
	//
	// // then
	// //assertAmount(result.getPower()).approximates(valueOf(2, SI.NEWTON));
	// }

	public void powerShouldBeZero() {
		// given
		Amount<Length> zero = Amount.valueOf(0, SI.METER);
		Position joint = new Position(zero, zero, zero);

		Amount<Power> quantity = Amount.valueOf(1, SI.WATT);

		ForcedVector3<Power> vecPlus = new ForcedVector3<Power>(joint, getDirection(Amount.ONE), quantity);
		ForcedVector3<Power> vecMinus = new ForcedVector3<Power>(joint, getDirection(Amount.ONE.opposite()), quantity);

		// when
		ForcedVector3<Power> result = vecPlus.add(vecMinus);

		// then
		assertAmount(result.getPower()).approximates(valueOf(0, SI.WATT));
	}

	private Vector3 getDirection(Amount<Dimensionless> value) {
		return new Vector3(value, value, value);
	}

	@DataProvider
	public Object[][] directedVectorsWithExpectedResults() {
		return new Object[][] { {

		} };
	}
}
