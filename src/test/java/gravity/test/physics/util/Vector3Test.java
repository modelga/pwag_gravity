package gravity.test.physics.util;

import gravity.physics.util.Vector3;

import javax.measure.quantity.Dimensionless;

import org.jscience.physics.amount.Amount;
import org.testng.annotations.Test;

import static gravity.test.assertion.AmountAssertion.assertAmount;

public class Vector3Test {

	@Test()
	public void shouldNormalizeToOne() {
		// given
		Amount<Dimensionless> one = Amount.ONE;
		Vector3 vector = new Vector3(one, one, one);

		// when
		Vector3 normalized = vector.normalize();

		// then
		assertAmount(normalized.getLength()).approximates(Amount.ONE);
	}

	@Test()
	public void positionShouldBeZero() {
		// given
		Amount<Dimensionless> metrPlus = Amount.ONE;
		Amount<Dimensionless> metrMinus = Amount.ONE.opposite();
		Vector3 vectorPlus = new Vector3(metrPlus, metrPlus, metrPlus);
		Vector3 vectorMinus = new Vector3(metrMinus, metrMinus, metrMinus);

		// when
		Vector3 result = vectorPlus.add(vectorMinus);

		// then
		assertAmount(result.getX()).approximates(Amount.ZERO);
		assertAmount(result.getY()).approximates(Amount.ZERO);
		assertAmount(result.getZ()).approximates(Amount.ZERO);
	}

	@Test()
	public void timesVector() {
		// given
		double multipler = 2;
		Amount<Dimensionless> value = Amount.ONE.times(multipler);
		Vector3 vector = new Vector3(value, value, value);

		// when
		Vector3 result = Vector3.times(vector, value);

		// then
		assertAmount(result.getX()).approximates(Amount.ONE.times(4));
	}

}
