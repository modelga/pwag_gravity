package gravity.test.data.celestialbody;

import gravity.physics.BodyStaticData;

import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public class Man extends BodyStaticData {
	public Man() {
		super("Man", Amount.valueOf(75, SI.KILOGRAM), Amount.valueOf(1.5, SI.METER));
	}
}
