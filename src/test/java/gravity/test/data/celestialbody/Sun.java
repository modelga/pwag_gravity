package gravity.test.data.celestialbody;

import gravity.physics.BodyStaticData;

import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public class Sun extends BodyStaticData {
	public Sun() {
		super("Sun", Amount.valueOf(1.9891, SI.GIGA(SI.YOTTA(SI.KILOGRAM))), Amount.valueOf(1.392, SI.MEGA(SI.KILOMETER)));
	}
}
