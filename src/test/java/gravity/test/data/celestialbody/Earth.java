package gravity.test.data.celestialbody;

import gravity.physics.BodyStaticData;

import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public class Earth extends BodyStaticData {
	public Earth() {
		super("Earth", Amount.valueOf(5.9736, SI.YOTTA(SI.KILOGRAM)), Amount.valueOf(6373.14, SI.KILOMETER));
	}
}
