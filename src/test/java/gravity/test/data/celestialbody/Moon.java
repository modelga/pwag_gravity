package gravity.test.data.celestialbody;

import gravity.physics.BodyStaticData;

import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public class Moon extends BodyStaticData {
	public Moon() {
		super("Moon", Amount.valueOf(73.47673, SI.ZETTA(SI.KILOGRAM)), Amount.valueOf(3476.2, SI.KILOMETER));
	}
}
