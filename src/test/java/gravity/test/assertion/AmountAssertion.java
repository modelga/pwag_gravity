package gravity.test.assertion;

import javax.measure.quantity.Quantity;

import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;
import org.jscience.physics.amount.Amount;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class AmountAssertion<T extends Quantity> extends GenericAssert<AmountAssertion, Amount<T>> {

	protected AmountAssertion(Amount<T> actual) {
		super(AmountAssertion.class, actual);
	}

	public AmountAssertion<T> approximates(Amount<? extends Quantity> value) {
		String message = String.format("%s isn't be approx to %s", actual, value);
		Assertions.assertThat(actual.approximates(value)).overridingErrorMessage(message).isTrue();
		return this;
	}

	public AmountAssertion<T> isGreaterThan(Amount<? extends Quantity> value) {
		String message = String.format("%s isn't greater than %s", actual, value);
		Assertions.assertThat(actual.isGreaterThan((Amount<T>) value)).overridingErrorMessage(message);
		return this;
	}

	public AmountAssertion<T> isLargerThan(Amount<? extends Quantity> value) {
		String message = String.format("%s isn't larger than %s", actual.abs(), value.abs());
		Assertions.assertThat(actual.isLargerThan((Amount<T>) value)).overridingErrorMessage(message);
		return this;
	}

	public AmountAssertion<T> isLessThan(Amount<? extends Quantity> value) {
		String message = String.format("%s isn't less than %s", actual, value);
		Assertions.assertThat(actual.isLessThan((Amount<T>) value)).overridingErrorMessage(message);
		return this;
	}

	public static AmountAssertion<? extends Quantity> assertAmount(Amount<? extends Quantity> amount) {
		return new AmountAssertion(amount);
	}
}
