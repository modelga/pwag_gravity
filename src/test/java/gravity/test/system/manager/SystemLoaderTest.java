package gravity.test.system.manager;

import gravity.physics.DynamicCelestialBody;
import gravity.system.manager.SystemLoader;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static gravity.test.assertion.AmountAssertion.assertAmount;

import static org.fest.assertions.Assertions.assertThat;

public class SystemLoaderTest {

	private final SystemLoader systemLoader = new SystemLoader();

	@Test(dataProvider = "jsonProvider")
	public void shouldCreateBodyFromJson(URL url) throws JsonParseException, JsonMappingException, IOException {
		// when
		Collection<DynamicCelestialBody> bodies = systemLoader.load(url);

		// then

		assertThat(bodies).isNotEmpty();
		DynamicCelestialBody firstBody = bodies.iterator().next();
		assertThat(firstBody.getBody().getName()).isNotEmpty();
		assertAmount(firstBody.getBody().getMass()).isGreaterThan(Amount.valueOf(0, SI.KILOGRAM));
	}

	@DataProvider
	public Object[][] jsonProvider() {
		ClassLoader classLoader = getClass().getClassLoader();

		return new Object[][] { { classLoader.getResource("json/sample.json") }, { classLoader.getResource("json/sample2.json") },
				{ classLoader.getResource("json/sample3.json") }, { classLoader.getResource("json/sample4.json") } };

	}
}
