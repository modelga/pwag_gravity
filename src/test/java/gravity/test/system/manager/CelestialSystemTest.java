package gravity.test.system.manager;

import gravity.system.api.DuplicateException;
import gravity.system.manager.CelestialSystem;
import gravity.system.manager.ParentResolver;
import gravity.system.manager.SystemLoader;

import java.io.IOException;
import java.net.URL;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static org.fest.assertions.Assertions.assertThat;

public class CelestialSystemTest {

	@Test(dataProvider = "jsonProvider")
	public void shouldCreateBodyFromJson(URL url) throws JsonParseException, JsonMappingException, IOException, DuplicateException {
		// given
		CelestialSystem system = new CelestialSystem(new SystemLoader(), null);
		ParentResolver parentResolver = new ParentResolver();
		system.addListener(parentResolver);
		system.addParentResolver(parentResolver);
		system.loadData(url);

		// when
		String json = system.toJSON();

		// then
		System.out.println(json);
		assertThat(json).isNotEmpty();
	}

	@DataProvider
	public Object[][] jsonProvider() {
		ClassLoader classLoader = getClass().getClassLoader();

		return new Object[][] { { classLoader.getResource("json/sample.json")

		} };

	}
}
