package gravity.test.system.manager;

import static gravity.test.assertion.AmountAssertion.assertAmount;
import static org.jscience.physics.amount.Amount.valueOf;
import gravity.physics.DynamicCelestialBody;
import gravity.physics.util.ForcedVector3;
import gravity.physics.util.Position;
import gravity.test.data.celestialbody.Earth;
import gravity.test.data.celestialbody.Sun;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;

import org.jscience.physics.amount.Amount;
import org.testng.annotations.Test;

public class DynamicCelestialBodyTest {

	@SuppressWarnings("unchecked")
	@Test(enabled = false)
	public void shouldDirectionVectorFromParentToChildAndVelocityVectorOfChildBeOrthogonal() {
		// given
		System.out.println("result: ");
		DynamicCelestialBody sun = new DynamicCelestialBody(new Sun(), new Position(0.0f, 0.0f, 0.0f));
		DynamicCelestialBody earth = new DynamicCelestialBody(new Earth(), new Position(1.0f, 1.0f, 1.0f), sun);
		Amount<Length> one = (Amount<Length>) valueOf(1, Length.UNIT.getStandardUnit());

		ForcedVector3<Length> toChildDirection = new ForcedVector3<Length>(sun.getPosition(), earth.getPosition(), one);
		ForcedVector3<Velocity> childVelocity = earth.getVelocityVector();
		// DirectionalVector3<Length> childVelocity = new
		// DirectionalVector3<Length>(new Position(0, 0, 0), new Position(1, 1,
		// -2), one);

		// when
		Amount<Dimensionless> productX = (Amount<Dimensionless>) toChildDirection.getX().times(childVelocity.getX());
		Amount<Dimensionless> productY = (Amount<Dimensionless>) toChildDirection.getY().times(childVelocity.getY());
		Amount<Dimensionless> productZ = (Amount<Dimensionless>) toChildDirection.getZ().times(childVelocity.getZ());
		Amount<Dimensionless> result = productX.plus(productY).plus(productZ);

		// then
		assertAmount(result).approximates(Amount.ZERO);
	}

}
