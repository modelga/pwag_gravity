package gravity.visual.api;

import gravity.conector.VisualPhysicsConnector;

public interface FocusListener {
	void onFocusChanged(VisualPhysicsConnector connector);
}
