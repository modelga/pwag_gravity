package gravity.visual;

import gravity.physics.DynamicCelestialBody;
import gravity.conector.VisualPhysicsConnector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;

public class CameraCustomInputController implements InputProcessor {
	Camera cam;
	private float movePitch = 0.0f;
	private float moveYaw = 0.0f;
	private float moveRoll = 0.0f;
	private int mouseButtonDown;
	private float rotatePitch = 0.0f;
	private float startX, startY;
	private Vector3 vecY = new Vector3();
	private Vector3 vecR = new Vector3();
	private Vector3 vecP = new Vector3();

	public CameraCustomInputController(final Camera camera) {
		cam = camera;
	}

	public boolean keyDown(int keycode) {
		if (Input.Keys.A == keycode) {
			movePitch = -10.0f;
		}
		if (Input.Keys.D == keycode) {
			movePitch = 10.0f;
		}
		if (Input.Keys.W == keycode) {
			moveYaw = -10.0f;
		}
		if (Input.Keys.S == keycode) {
			moveYaw = 10.0f;
		}
		if (Input.Keys.SHIFT_LEFT == keycode) {
			moveRoll = 10.0f;
		}
		if (Input.Keys.CONTROL_LEFT == keycode) {
			moveRoll = -10.0f;
		}
		if (Input.Keys.Q == keycode) {
			rotatePitch = 20.0f;
		}
		if (Input.Keys.E == keycode) {
			rotatePitch = -20.0f;
		}
		return false;
	}

	public boolean keyUp(int keycode) {
		if (Input.Keys.A == keycode) {
			movePitch = 0.0f;
		}
		if (Input.Keys.D == keycode) {
			movePitch = 0.0f;
		}
		if (Input.Keys.W == keycode) {
			moveYaw = 0.0f;
		}
		if (Input.Keys.S == keycode) {
			moveYaw = 0.0f;
		}
		if (Input.Keys.SHIFT_LEFT == keycode) {
			moveRoll = 0.0f;
		}
		if (Input.Keys.CONTROL_LEFT == keycode) {
			moveRoll = 0.0f;
		}
		if (Input.Keys.Q == keycode) {
			rotatePitch = 0.0f;
		}
		if (Input.Keys.E == keycode) {
			rotatePitch = 0.0f;
		}
		return false;
	}

	public boolean keyTyped(char character) {
		return false;
	}

	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		mouseButtonDown = button;
		if (button == Input.Buttons.RIGHT) {
			startX = screenX;
			startY = screenY;
			return true;
		}
		return false;
	}

	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (mouseButtonDown == Input.Buttons.RIGHT) {
			cam.rotate(cam.up, (startX - screenX) * 0.5f);

			Vector3 rotateAxis = new Vector3(cam.direction);
			rotateAxis.rotate(cam.up, -90.0f);
			cam.rotate(rotateAxis, (startY - screenY) * 0.5f);

			startX = screenX;
			startY = screenY;
		}
		return false;
	}

	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	public boolean scrolled(int amount) {
		return false;
	}

	public void update() {
		final float delta = Gdx.graphics.getDeltaTime();
		if (movePitch != 0.0f) {
			cam.translate(vecP.set(cam.direction).crs(cam.up).nor().scl(delta * movePitch));
		}
		if (moveYaw != 0.0f) {
			cam.translate(vecY.set(cam.direction).scl(delta * -moveYaw));
		}
		if (moveRoll != 0.0f) {
			cam.translate(vecR.set(cam.up).scl(delta * moveRoll));
		}
		if (rotatePitch != 0.0f) {
			cam.rotate(cam.direction, delta * rotatePitch);
		}
	}

	public void setCamera(DynamicCelestialBody body) {
		// TOD check distance from camera?
		// When planet i small they are to close camera
		// and opposite when is too big
		float bodyRadius = (float) (body.getBody().getRadius().getEstimatedValue() / VisualPhysicsConnector.multiplySIMeterToDouble);
		float newX = (float) (body.getAbsolutePosition().getX().getEstimatedValue() / VisualPhysicsConnector.multiplySIMeterToDouble);
		float newY = (float) (body.getAbsolutePosition().getY().getEstimatedValue() / VisualPhysicsConnector.multiplySIMeterToDouble);
		float newZ = (float) (body.getAbsolutePosition().getZ().getEstimatedValue() / VisualPhysicsConnector.multiplySIMeterToDouble);
		cam.lookAt(newX, newY, newZ);
		newX -= 2 * bodyRadius * cam.direction.x;
		newY -= 2 * bodyRadius * cam.direction.y;
		newZ -= 2 * bodyRadius * cam.direction.z;
		cam.position.set(newX, newY, newZ);
		System.out.println(String.format("%s; %s", cam.position, cam.direction));
	}

}
