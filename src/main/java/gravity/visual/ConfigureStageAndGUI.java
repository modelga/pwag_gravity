package gravity.visual;

import gravity.conector.VisualPhysicsArray;
import gravity.physics.BodyStaticData;
import gravity.physics.DynamicCelestialBody;
import gravity.physics.util.Position;
import gravity.system.api.DuplicateException;
import gravity.system.io.LoadListener;
import gravity.system.io.SaveListener;
import gravity.system.manager.CelestialSystem;
import gravity.visual.utils.*;

import javax.measure.quantity.Length;
import javax.measure.quantity.Mass;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

public class ConfigureStageAndGUI implements EventListener {

	TextButton createButton;
	TextButton saveButton;
	TextButton loadButton;
	TextButton cleanButton;
	TimeSlider timeSlider;
	Label timeMultiplerLabel;
	ComboBox bodyComboBox;
	Model model;
	Stage stage;
	ModelInstance instance;
	ModelBuilder modelBuilder = new ModelBuilder();
	public Array<ModelInstance> instances = new Array<ModelInstance>();
	GuiTextFieldBuilder guiBuilder;
	public VisualPhysicsArray celestialArray;
	private final CelestialSystem system;
	private CameraCustomInputController cameraController;
	private FocusedInfoGroup focusedInfoGroup;

	public ConfigureStageAndGUI(CelestialSystem system, CameraCustomInputController cameraController) {
		this.system = system;
		this.cameraController = cameraController;
		celestialArray = new VisualPhysicsArray(system);
		system.addListener(celestialArray);
		guiBuilder = new GuiTextFieldBuilder();
		setButtons();
		stage = guiBuilder.getStage();
		stage.addActor(createButton);
		stage.addActor(saveButton);
		stage.addActor(loadButton);
		stage.addActor(cleanButton);
		setTimeSlider();
		stage.addActor(timeSlider);
		setTimeMultiplerLabel();
		stage.addActor(timeMultiplerLabel);
		bodyComboBox = new ComboBox();
		system.addListener(bodyComboBox);
		bodyComboBox.setItems(celestialArray.getDynamicCelestialBodyArray());
		bodyComboBox.addListener(this);
		stage.addActor(bodyComboBox);
		stage.addActor(focusedInfoGroup);
	}

	private void setButtons() {
		setCreateButton();
		setSaveButton();
		setLoadButton();
		setCleanButton();
		setFocusedInfoGroup();
	}

	private void setFocusedInfoGroup() {
		focusedInfoGroup = new FocusedInfoGroup();
		focusedInfoGroup.setBounds(ComponentSize.border, (ComponentSize.height), ComponentSize.width, ComponentSize.height);
	}

	private void setCreateButton() {
		createButton = new TextButton("CREATE", new ButtonStyleBuilder().create());
		createButton.setBounds(Gdx.graphics.getWidth() - ComponentSize.border - ComponentSize.width, (int) ((double) ComponentSize.height / 2),
				ComponentSize.width, ComponentSize.height);
		createButton.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (createCelestialBodyFromInputs()) {
					guiBuilder.clearTextFields();
				}
				return true;
			}
		});
	}

	private void setSaveButton() {
		saveButton = new TextButton("SAVE", new ButtonStyleBuilder().create());
		saveButton.setBounds(ComponentSize.border, Gdx.graphics.getHeight() - ComponentSize.border - ComponentSize.height, ComponentSize.width,
				ComponentSize.height);
		saveButton.addListener(new SaveListener(system));
	}

	private void setLoadButton() {
		loadButton = new TextButton("LOAD", new ButtonStyleBuilder().create());
		loadButton.setBounds(ComponentSize.border + ComponentSize.width, Gdx.graphics.getHeight() - ComponentSize.border - ComponentSize.height,
				ComponentSize.width, ComponentSize.height);
		loadButton.addListener(new LoadListener(system));
	}

	private void setCleanButton() {
		cleanButton = new TextButton("CLEAN", new ButtonStyleBuilder().create());
		cleanButton.setBounds(ComponentSize.border + (ComponentSize.width * 2), Gdx.graphics.getHeight() - ComponentSize.border - ComponentSize.height,
				ComponentSize.width, ComponentSize.height);
		cleanButton.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				system.removeAll();
				return true;
			}
		});
	}

	private void setTimeSlider() {
		timeSlider = new TimeSlider();
	}

	private void setTimeMultiplerLabel() {
		timeMultiplerLabel = new Label("", new LabelStyleBuilder().create());
		timeMultiplerLabel.setBounds(timeSlider.getX() + timeSlider.getWidth(), Gdx.graphics.getHeight() - ComponentSize.border
				- (int) (ComponentSize.height * 1.75), ComponentSize.width / 2, ComponentSize.height);
	}

	private boolean createCelestialBodyFromInputs() {
		DynamicCelestialBody dynaBody = doCreateBody();
		return doAddBodyToSystem(dynaBody);
	}

	private boolean doAddBodyToSystem(DynamicCelestialBody dynaBody) {
		try {
			system.addBody(dynaBody);
			cameraController.setCamera(dynaBody); // TOD here camera is set
			return true;
		} catch (final DuplicateException e) {
			system.waitForEnd(new Runnable() {
				@Override
				public void run() {
					showMessageDialog(null, e.getMessage(), "Error", ERROR_MESSAGE);
				}
			});
			return false;
		}
	}

	private DynamicCelestialBody doCreateBody() {
		DynamicCelestialBody dynaBody;
		double massValue = parseDString(guiBuilder.mass.getText());
		double radiusValue = parseDString(guiBuilder.radius.getText());
		double posXValue = (parseDString(guiBuilder.posInAxisX.getText()));
		double posYValue = (parseDString(guiBuilder.posInAxisY.getText()));
		double posZValue = (parseDString(guiBuilder.posInAxisZ.getText()));
		Amount<Mass> mass = Amount.valueOf(massValue, SI.KILOGRAM);
		Amount<Length> radius = Amount.valueOf(radiusValue, SI.METER);
		BodyStaticData body = new BodyStaticData(guiBuilder.name.getText(), mass, radius);
		Position position = new Position(posXValue, posYValue, posZValue);
		if (guiBuilder.primeObject.getText().isEmpty()) {
			dynaBody = new DynamicCelestialBody(body, position);
		} else {
			dynaBody = new DynamicCelestialBody(body, position, guiBuilder.primeObject.getText(), null, false);
		}

		return dynaBody;
	}

	public Stage getConfuguredGuiOnStage() {
		return this.stage;
	}

	private double parseDString(String strToParse) {
		if (!strToParse.isEmpty()) {
			if (strToParse.contains("e") || strToParse.contains("E")) {
				return parseDoubleE(strToParse);
			}
			return Double.parseDouble(strToParse);
		} else {
			return 0.0;
		}
	}

	private double parseDoubleE(String s) {
		String separator = "";
		if (s.contains("e")) {
			separator = "e";
		} else if (s.contains("E")) {
			separator = "E";
		}

		String[] tokens = s.split(separator);
		if (tokens.length == 2) {
			double a = Double.parseDouble(tokens[0]);
			int b = (int) Double.parseDouble(tokens[1]);
			double multipler = Math.pow(10.0, b);
			if (b >= 0) {
				return a * multipler;
			} else {
				return a / multipler;
			}
		}
		return Double.parseDouble(tokens[0]);
	}

	public FocusedInfoGroup getFocusedInfoGroup() {
		return focusedInfoGroup;
	}

	/**
	 * Call method if gui should be updated e.g update frame
	 * 
	 */
	public void updateGUI() {
		updateTimeMultiplerLabel();
		checkComboBox();
		focusedInfoGroup.update();
	}

	private void updateTimeMultiplerLabel() {
		timeMultiplerLabel.setText(timeSlider.getLabelText());
	}

	private void checkComboBox() {
		bodyComboBox.setVisible(bodyComboBox.getItems().size > 0);
	}

	public void unfocusTextFields() {
		guiBuilder.unfocusTextFields();
	}

	@Override
	public boolean handle(Event arg0) {
		if (arg0 instanceof ChangeEvent) {
			cameraController.setCamera(bodyComboBox.getSelection().first());
			System.out.println(String.format("wybrane: %s", bodyComboBox.getSelection().first().getBody().getName()));
		}
		return false;
	}
}