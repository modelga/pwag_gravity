package gravity.visual.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Slider.SliderStyle;

public class SliderStyleBuilder {
	private SliderStyle style = new SliderStyle();

	public SliderStyleBuilder() {
		NinePatch bg = new NinePatch(new Texture(Gdx.files.classpath("slider_bg.png")), 3, 3, 0, 0);
		NinePatch knob = new NinePatch(new Texture(Gdx.files.classpath("slider_knob.png")), 0, 0, 3, 3);
		style.background = new Image(bg).getDrawable();
		style.knob = new Image(knob).getDrawable();
	}

	public SliderStyle create() {
		return this.style;
	}
}
