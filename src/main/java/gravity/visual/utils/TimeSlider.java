package gravity.visual.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;

public class TimeSlider extends Slider {

	private static final int width = ComponentSize.width * 3;
	private static final int height = 20;

	private static final float minValue = 0.0f;
	private static final float stepSize = 1.0f;
	private static final boolean isVertical = false;

	private static final float minute = 60;
	private static final float hour = minute * 60;
	private static final float day = hour * 24;
	private static final float week = day * 7;
	private static final float month = day * 31;
	private static final float year = day * 365.25f;
	private static final float maxValue = year * 10;

	// TOD have to decide how much user can increase time
	// higher value gives a greater errors

	public TimeSlider() {
		super(minValue, width, stepSize, isVertical, new SliderStyleBuilder().create());
		this.setBounds(ComponentSize.border, Gdx.graphics.getHeight() - (ComponentSize.border * 2) - ComponentSize.height - height, width, height);
	}

	public float getMultipler() {
		float value = getValue();
		float intervalSize = getWidth() / 7;
		int interval = (int) (value / intervalSize);
		if (interval > 6) {
			interval = 6;
		}
		value -= interval * intervalSize;
		if (interval == 0) {
			value = valueInInterval(value, 0.0f, minute, intervalSize);
		} else if (interval == 1) {
			value = valueInInterval(value, minute, hour, intervalSize);
		} else if (interval == 2) {
			value = valueInInterval(value, hour, day, intervalSize);
		} else if (interval == 3) {
			value = valueInInterval(value, day, week, intervalSize);
		} else if (interval == 4) {
			value = valueInInterval(value, week, month, intervalSize);
		} else if (interval == 5) {
			value = valueInInterval(value, month, year, intervalSize);
		} else {
			value = valueInInterval(value, year, maxValue, intervalSize);
		}
		return value;
	}

	private float valueInInterval(float x, float min, float max, float intervalSize) {
		return ((max - min) / intervalSize) * x + min;
	}

	public String getLabelText() {
		float value = getMultipler();
		if (value < minute) {
			return (String.format("%.2f second", value));
		} else if (value < hour) {
			return (String.format("%.2f minute", value / minute));
		} else if (value < day) {
			return (String.format("%.2f hour", value / hour));
		} else if (value < week) {
			return (String.format("%.2f day", value / day));
		} else if (value < month) {
			return (String.format("%.2f week", value / week));
		} else if (value < year) {
			return (String.format("%.2f month", value / month));
		} else {
			return (String.format("%.2f year", value / year));
		}
	}
}
