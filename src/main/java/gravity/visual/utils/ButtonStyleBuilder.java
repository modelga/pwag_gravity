package gravity.visual.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class ButtonStyleBuilder {
	private TextButtonStyle style = new TextButtonStyle();

	public ButtonStyleBuilder() {
		NinePatch patch = new NinePatch(new Texture(Gdx.files.classpath("textField3.png")), 6, 6, 6, 6);
		NinePatch patchOff = new NinePatch(new Texture(Gdx.files.classpath("textField2.png")), 6, 6, 6, 6);
		style.up = new Image(patch).getDrawable();
		style.down = new Image(patchOff).getDrawable();
		style.font = new BitmapFont();
		style.font.setScale(1.1f);
		style.fontColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public TextButtonStyle create() {
		return this.style;
	}
}
