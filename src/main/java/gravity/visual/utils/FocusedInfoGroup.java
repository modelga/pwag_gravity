package gravity.visual.utils;

import gravity.conector.VisualPhysicsConnector;
import gravity.physics.BodyStaticData;
import gravity.physics.DynamicCelestialBody;
import gravity.physics.util.Position;
import gravity.visual.api.FocusListener;

import javax.measure.quantity.Quantity;

import org.jscience.physics.amount.Amount;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

public class FocusedInfoGroup extends HorizontalGroup implements FocusListener {

	private Label name;
	private Label radius;
	private Label position;
	private Label mass;
	private Label velocity;
	private VisualPhysicsConnector focus;

	public FocusedInfoGroup() {
		super();
		VerticalGroup labels = createLabels();
		VerticalGroup values = createValues();
		setVisible(true);
		addActor(labels);
		addActor(values);
	}

	private VerticalGroup createValues() {
		VerticalGroup group = new VerticalGroup();
		LabelStyle labelStyle = new LabelStyleBuilder().create();
		name = new Label("", labelStyle);
		radius = new Label("", labelStyle);
		position = new Label("", labelStyle);
		mass = new Label("", labelStyle);
		velocity = new Label("", labelStyle);
		addActorsToGroup(group, name, radius, position, mass, velocity);
		return group.left();
	}

	private VerticalGroup createLabels() {
		VerticalGroup group = new VerticalGroup();
		LabelStyle labelStyle = new LabelStyleBuilder().create();
		group.addActor(new Label("Name:", labelStyle));
		group.addActor(new Label("Radius:", labelStyle));
		group.addActor(new Label("Position:", labelStyle));
		group.addActor(new Label("Mass:", labelStyle));
		group.addActor(new Label("Velocity:", labelStyle));
		return group.left();
	}

	private void addActorsToGroup(Group group, Actor... actors) {
		for (Actor actor : actors) {
			group.addActor(actor);
		}
	}

	public void update() {
		updateValues(focus);
	}

	// TOD rename method name
	private String getAmount(Amount<? extends Quantity> amount) {
		return amount.getEstimatedValue() + amount.getUnit().toString();
	}

	private void updateValues(VisualPhysicsConnector connector) {
		if (connector != null) {
			setVisible(true);
			DynamicCelestialBody dynamicBody = connector.getDynamicBody();
			BodyStaticData body = dynamicBody.getBody();
			name.setText(body.getName());
			radius.setText(getAmount(body.getRadius()));
			// TOD rename method name
			position.setText(getPosition(dynamicBody));
			mass.setText(getAmount(body.getMass()));
			velocity.setText(getAmount(dynamicBody.getVelocityVector().getPower()));

		} else {
			setVisible(false);
		}
	}

	private String getPosition(DynamicCelestialBody dynamicBody) {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("x: ");
		Position position2 = dynamicBody.getPosition();
		sBuilder.append(getAmount(position2.getX()));
		sBuilder.append(", y: ");
		sBuilder.append(getAmount(position2.getY()));
		sBuilder.append(", z: ");
		sBuilder.append(getAmount(position2.getZ()));
		return sBuilder.toString();
	}

	@Override
	public void onFocusChanged(VisualPhysicsConnector connector) {
		if (connector != null) {
			System.out.println("New focus on " + connector.getDynamicBody().getBody().getName());
		}
		this.focus = connector;
	}
}
