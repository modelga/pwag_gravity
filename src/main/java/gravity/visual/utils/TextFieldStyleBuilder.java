package gravity.visual.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;

public class TextFieldStyleBuilder extends TextFieldStyle {
	private TextFieldStyle style = new TextFieldStyle();
	private BitmapFont font = new BitmapFont();

	public TextFieldStyleBuilder() {
		NinePatch patch = new NinePatch(new Texture(Gdx.files.classpath("textField.png")), 6, 6, 6, 6);
		style.background = new Image(patch).getDrawable();
		style.font = font;
		style.font.setScale(1.1f);
		style.fontColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public TextFieldStyle create() {
		return this.style;
	}
}
