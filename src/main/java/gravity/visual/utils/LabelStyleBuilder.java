package gravity.visual.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class LabelStyleBuilder {
	private LabelStyle style = new LabelStyle();

	public LabelStyleBuilder() {
		style.font = new BitmapFont();
		style.font.setScale(1.1f);
		style.fontColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	}

	public LabelStyle create() {
		return this.style;
	}
}
