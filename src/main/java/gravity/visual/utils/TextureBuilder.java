package gravity.visual.utils;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;

public class TextureBuilder {
	private String planetDir = "planets/";
	private String sunDir = "suns/";

	public TextureBuilder() {
	}

	public TextureAttribute getPlanetTexture() {
		Texture texture = new Texture(Gdx.files.classpath(planetDir + "planet" + randomInt(34) + ".jpg"));
		return TextureAttribute.createDiffuse(texture);
	}

	public TextureAttribute getSunTexture() {
		Texture texture = new Texture(Gdx.files.classpath(sunDir + "sun" + randomInt(5) + ".jpg"));
		return TextureAttribute.createDiffuse(texture);
	}

	private int randomInt(int b) {
		Random rand = new Random();
		return (int) ((b) * (rand.nextFloat()));
	}
}
