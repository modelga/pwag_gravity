package gravity.visual.utils;

import java.util.regex.Pattern;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class GuiTextFieldBuilder {
	public TextField posInAxisX;
	public TextField posInAxisY;
	public TextField posInAxisZ;
	public TextField mass;
	public TextField radius;
	public TextField name;
	public TextField primeObject;
	Stage stage;

	private static String scientificNotationRegexp = "(-?\\d+\\.?\\d*)|(-?\\d*)|(-?\\d+(\\.\\d+)?[eE]-?\\d+)|(-?\\d+(\\.\\d+)?[eE]-?)|(-?\\d+(\\.\\d+)?[eE])|(-?\\d+(\\.\\d+)?)|(-?\\d+\\.?)|(-?\\d+\\.?)|(-?)";

	// TOD Regex looks really complicated, simplify is needed. But now work
	// properly.

	public GuiTextFieldBuilder() {
		setTextFieldsForCelesialBody();
		setTextFields();
		setListenersToTextFields();
		setFiltersToTextFields();
		setStage();
	}

	private void setFiltersToTextFields() {
		numericFilter(posInAxisX);
		numericFilter(posInAxisY);
		numericFilter(posInAxisZ);
		numericFilter(mass);
		numericFilter(radius);

	}

	void setStage() {
		//stage = new Stage(Gdx.app.getGraphics().getWidth(), Gdx.app.getGraphics().getHeight(), true);
		stage = new Stage();
		stage.clear();
		stage.addActor(name);
		stage.addActor(posInAxisX);
		stage.addActor(posInAxisY);
		stage.addActor(posInAxisZ);
		stage.addActor(mass);
		stage.addActor(radius);
		stage.addActor(primeObject);

	}

	private void setListenersToTextFields() {
		TextFieldListener(name);
		TextFieldListener(posInAxisX);
		TextFieldListener(posInAxisY);
		TextFieldListener(posInAxisZ);
		TextFieldListener(mass);
		TextFieldListener(radius);
		TextFieldListener(primeObject);
	}

	private void setTextFieldsForCelesialBody() {
		name = new TextField("", new TextFieldStyleBuilder().create());
		posInAxisX = new TextField("", new TextFieldStyleBuilder().create());
		posInAxisY = new TextField("", new TextFieldStyleBuilder().create());
		posInAxisZ = new TextField("", new TextFieldStyleBuilder().create());
		mass = new TextField("", new TextFieldStyleBuilder().create());
		radius = new TextField("", new TextFieldStyleBuilder().create());
		primeObject = new TextField("", new TextFieldStyleBuilder().create());
		primeObject.setDisabled(true);
		name.setMessageText("Name");
		posInAxisX.setMessageText("Position X in m");
		posInAxisY.setMessageText("Position Y in m");
		posInAxisZ.setMessageText("Position Z in m");
		mass.setMessageText("Mass in kg");
		radius.setMessageText("Radius in m");
		primeObject.setMessageText("Prime Object");
	}

	private void setTextFields() {
		float posX = Gdx.graphics.getWidth() - ComponentSize.border - ComponentSize.width;
		name.setBounds(posX, 8 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		posInAxisX.setBounds(posX, 7 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		posInAxisY.setBounds(posX, 6 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		posInAxisZ.setBounds(posX, 5 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		mass.setBounds(posX, 4 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		radius.setBounds(posX, 3 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
		primeObject.setBounds(posX, 2 * ComponentSize.height, ComponentSize.width, ComponentSize.height);
	}

	public Stage getStage() {
		return this.stage;
	}

	public void TextFieldListener(final TextField textfield) {
		textfield.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				stage.setKeyboardFocus(textfield);
				textfield.setTextFieldListener(new TextFieldListener() {
					@Override
					public void keyTyped(TextField textField, char key) {
					}
				});
				return true;
			}
		});
	}

	public void numericFilter(final TextField textfield) {
		TextField.TextFieldFilter filter = new TextFieldFilter() {

			@Override
			public boolean acceptChar(TextField textField, char key) {
				return Pattern.matches(scientificNotationRegexp, (textField.getText() + key));
			}
		};
		textfield.setTextFieldFilter(filter);
	}

	private void unfocus(TextField a, String s) {
		a.setText(s);
		stage.unfocus(a);
	}

	public void clearTextFields() {
		unfocus(name, "");
		unfocus(posInAxisX, "");
		unfocus(posInAxisY, "");
		unfocus(posInAxisZ, "");
		unfocus(mass, "");
		unfocus(radius, "");
		unfocus(primeObject, "");
	}

	public void unfocusTextFields() {
		for (Actor actor : new Actor[] { name, posInAxisX, posInAxisY, posInAxisZ, mass, radius, primeObject }) {
			stage.unfocus(actor);
		}
	}
}