package gravity.visual.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;

public class SelectBoxStyleBuilder {
	private SelectBoxStyle style = new SelectBoxStyle();

	public SelectBoxStyleBuilder() {
		style.font = new BitmapFont();
		style.font.setScale(1.1f);
		style.fontColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
		NinePatch bg = new NinePatch(new Texture(Gdx.files.classpath("textField3.png")), 6, 6, 6, 6);
		style.background = new Image(bg).getDrawable();
		style.backgroundOpen = new Image(bg).getDrawable();
		style.backgroundDisabled = new Image(bg).getDrawable();
		style.backgroundOver = new Image(bg).getDrawable();
		style.disabledFontColor = new Color(0.0f, 0.0f, 0.0f, 1.0f);
		style.scrollStyle = new ScrollPaneStyle(new Image(bg).getDrawable(), new Image(bg).getDrawable(), new Image(bg).getDrawable(), new Image(bg).getDrawable(), new Image(bg).getDrawable());
		style.listStyle = new ListStyle(style.font, new Color(1.0f, 1.0f, 1.0f, 1.0f), new Color(0.5f, 0.5f, 0.5f, 1.0f), new Image(bg).getDrawable());
	}

	public SelectBoxStyle create() {
		return this.style;
	}
}
