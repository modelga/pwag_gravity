package gravity.visual.utils;

import java.util.Collection;

import gravity.physics.DynamicCelestialBody;
import gravity.system.api.CelestialBodyLifeCycleListener;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.utils.Array;

public class ComboBox extends SelectBox<DynamicCelestialBody> implements CelestialBodyLifeCycleListener {
	public ComboBox() {
		super(new SelectBoxStyleBuilder().create());
		this.setBounds(Gdx.graphics.getWidth() - ComponentSize.border - ComponentSize.width, Gdx.graphics.getHeight() - ComponentSize.border
				- ComponentSize.height, ComponentSize.width, ComponentSize.height);
	}

	@Override
	public void addBody(DynamicCelestialBody body) {
		System.out.println(String.format("%s", body.getBody().getName()));
		Array<DynamicCelestialBody> old = this.getItems();
		Array<DynamicCelestialBody> newItems = new Array<>(old);
		newItems.add(body);
		this.setItems(newItems);
		return;
	}

	@Override
	public void addBody(Collection<DynamicCelestialBody> bodies) {
	}

	@Override
	public void removeBody(DynamicCelestialBody body) {
	}

	@Override
	public void removeAll() {
		Array<DynamicCelestialBody> emptyArray = new Array<>(DynamicCelestialBody.class);
		this.setItems(emptyArray);
	}

}
