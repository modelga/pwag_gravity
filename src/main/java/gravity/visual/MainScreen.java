package gravity.visual;

import gravity.conector.VisualPhysicsConnector;
import gravity.physics.DynamicCelestialBody;
import gravity.physics.PrecisePhysicsUpdater;
import gravity.system.manager.CelestialSystem;
import gravity.system.manager.ParentResolver;
import gravity.system.manager.SystemLoader;
import gravity.visual.api.FocusListener;
import gravity.visual.utils.ComponentSize;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class MainScreen implements ApplicationListener {
	public static String LOG = MainScreen.class.getSimpleName();
	public PerspectiveCamera camera3d;
	public CameraCustomInputController camController;
	private Environment environment;
	public ModelBatch modelBatch;
	ConfigureStageAndGUI gui;
	Stage stage;
	DynamicCelestialBody[] celestialBodies;
	private CelestialSystem system;
	private List<FocusListener> focusListeners;

	@Override
	public void create() {
		this.system = createCelestialSystem();
		setupCamera();
		focusListeners = new ArrayList<FocusListener>();
		this.gui = new ConfigureStageAndGUI(system, camController);
		this.stage = gui.getConfuguredGuiOnStage();
		Gdx.app.log(MainScreen.LOG, "Utworzenie okna");
		this.modelBatch = new ModelBatch();
		setEnvoirment();
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(0, stage);
		inputMultiplexer.addProcessor(1, camController);
		Gdx.input.setInputProcessor(inputMultiplexer);
		focusListeners.add(gui.getFocusedInfoGroup());
	}

	public void registerFocusListener(FocusListener listener) {
		focusListeners.add(listener);
	}

	public void focusChanged(VisualPhysicsConnector connector) {
		for (FocusListener focusListener : focusListeners) {
			focusListener.onFocusChanged(connector);
		}
	}

	private void setupCamera() {
		setCamera();
		this.camController = new CameraCustomInputController(camera3d);
	}

	private CelestialSystem createCelestialSystem() {
		CelestialSystem system = new CelestialSystem(new SystemLoader(), new PrecisePhysicsUpdater(12*3600.0));
		ParentResolver parentResolver = new ParentResolver();
		system.addListener(parentResolver);
		system.addParentResolver(parentResolver);
		return system;
	}

	private void setEnvoirment() {
		environment = new Environment();
		environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
	}

	private void setCamera() {
		camera3d = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera3d.position.set(0f, 0f, 10f);
		camera3d.lookAt(0.0f, 0.0f, 0.0f);
		camera3d.up.set(0.0f, 1.0f, 0.0f);
		camera3d.near = 0.01f;
		camera3d.far = 30000f;
		camera3d.update();
	}

	@Override
	public void render() {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		camController.update();
		camera3d.update();
		stage.act();

		modelBatch.begin(camera3d);
		modelBatch.render(gui.celestialArray.getInstances(), environment);
		modelBatch.end();
		updateLights();
		intersectWithObjects();
		moveObjects();
		gui.updateGUI();
		stage.draw();
	}

	private void updateLights() {
		environment.pointLights.clear();
		for (VisualPhysicsConnector connector : gui.celestialArray.getVisualPhysicsConnectors()) {
			DynamicCelestialBody dynamicBody = connector.getDynamicBody();
			if (dynamicBody.isSun()) {
				double[] position = VisualPhysicsConnector.translatePosition(dynamicBody.getPosition());
				float lightPower = (float) ((dynamicBody.getBody().getMass().getEstimatedValue() / Math.pow(dynamicBody.getBody().getRadius()
						.getEstimatedValue(), 2)) / 10000);
				environment.add(new PointLight().set(254, 255, 199, (float) position[0], (float) position[1], (float) position[2],
 lightPower));
			}
		}
	}

	private void moveObjects() {
		double deltaTime = Gdx.graphics.getDeltaTime() * gui.timeSlider.getMultipler();
		system.update(deltaTime);
		updateVisualPosition();
	}

	private void updateVisualPosition() {

		for (VisualPhysicsConnector connector : gui.celestialArray.getVisualPhysicsConnectors()) {
			Vector3 position = gui.celestialArray.getPosition(connector.getDynamicBody());
			connector.getVisualBody().transform.setTranslation(position);
		}
	}

	private void intersectWithObjects() {
		Ray pickRay = null;
		if (Gdx.input.isTouched() && Gdx.input.isButtonPressed(Input.Buttons.LEFT) && !insideGui()) {
			gui.unfocusTextFields();
			pickRay = camera3d.getPickRay(Gdx.input.getX(), Gdx.input.getY(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			// Gdx.app.log("PickingTest", "ray: " + pickRay);
			Vector3 intersection = new Vector3();
			Vector3 tempVector = new Vector3();
			focusChanged(null);
			for (VisualPhysicsConnector connector : gui.celestialArray.getVisualPhysicsConnectors()) {
				connector.getVisualBody().transform.getTranslation(tempVector);
				if (isPickIntersectWithObject(pickRay, intersection, connector.getDynamicBody(), tempVector)) {
					focusChanged(connector);
					gui.guiBuilder.primeObject.setText(gui.celestialArray.getNameOfSphere(connector.getDynamicBody()));
					break;
				} else {
					gui.guiBuilder.primeObject.setText("");
				}
			}
		}
	}

	private boolean isPickIntersectWithObject(Ray pickRay, Vector3 intersection, DynamicCelestialBody body, Vector3 tempVector) {
		float radiusOfSphere = gui.celestialArray.getRadiusOfSphere(body);
		return pickRay != null && Intersector.intersectRaySphere(pickRay, tempVector, radiusOfSphere, intersection);
	}

	private boolean insideGui() {
		if ((Gdx.input.getX() > Gdx.graphics.getWidth() - ComponentSize.border - ComponentSize.width)
				&& (Gdx.input.getX() < Gdx.graphics.getWidth() - ComponentSize.border)) {
			if (((Gdx.graphics.getHeight() - Gdx.input.getY()) > 2 * ComponentSize.height)
					&& ((Gdx.graphics.getHeight() - Gdx.input.getY()) < 9 * ComponentSize.height)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		modelBatch.dispose();
		stage.dispose();
	}

}
