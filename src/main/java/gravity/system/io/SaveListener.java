package gravity.system.io;

import gravity.system.manager.CelestialSystem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import static javax.swing.JFileChooser.APPROVE_OPTION;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

final public class SaveListener extends ClickListener {
	private final CelestialSystem system;
	private JFileChooser chooser;

	public SaveListener(CelestialSystem system) {
		this.system = system;
	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		if (system.getBodies().size() == 0) {
			showMessageDialog(null, "There is nothing to save!", "Information", INFORMATION_MESSAGE);
			return false;
		} else {
			return chooseFileAndSave();
		}
	}

	private boolean chooseFileAndSave() {
		chooser = new CustomJFileChooser();
		system.waitForEnd(new Runnable() {
			public void run() {
				do {
					try {
						save(chooser.showSaveDialog(null));
						break;
					} catch (Throwable e) {
					}
				} while (true);
			}
		});
		return true;
	}

	private void save(int returnValue) throws Exception {
		if (returnValue == APPROVE_OPTION) {
			try {
				File file = chooser.getSelectedFile();
				doSave(file);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Error", ERROR_MESSAGE);
				throw new Exception("Save failed, retry!");
			}
		}
	}

	private void doSave(File file) throws IOException {
		FileWriter fileWriter = new FileWriter(file);
		fileWriter.write(system.toJSON());
		fileWriter.close();
		showMessageDialog(null, "Data stored in: " + file.getAbsolutePath(), "Data was successfully saved!", INFORMATION_MESSAGE);
	}
}