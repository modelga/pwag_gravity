package gravity.system.io;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.YES_NO_OPTION;
import static javax.swing.JOptionPane.YES_OPTION;

public class CustomJFileChooser extends JFileChooser {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8321126110473406223L;

	@Override
	public void approveSelection() {
		if (getDialogType() == SAVE_DIALOG) {
			if (!getSelectedFile().exists() || checkIfOverwrite(getSelectedFile()) == YES_OPTION) {
				super.approveSelection();
			}
		} else {
			super.approveSelection();
		}
	}

	static private int checkIfOverwrite(File file) {
		return JOptionPane.showConfirmDialog(null, "File '" + file.getName() + "' exists!\r\n" + "Do you want to overwrite these file?", "Are you sure?",
				YES_NO_OPTION, INFORMATION_MESSAGE);
	}
}
