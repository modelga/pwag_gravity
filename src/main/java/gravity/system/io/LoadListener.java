package gravity.system.io;

import gravity.system.api.DuplicateException;
import gravity.system.manager.CelestialSystem;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFileChooser;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import static java.lang.String.format;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;

final public class LoadListener extends ClickListener {
	private final CelestialSystem system;
	private JFileChooser chooser;

	public LoadListener(CelestialSystem system) {
		this.system = system;
	}

	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		chooser = new JFileChooser();
		system.waitForEnd(new Runnable() {
			@Override
			public void run() {
				do {
					try {
						load(chooser.showOpenDialog(null));
						break;
					} catch (Throwable e) {
						showMessageDialog(null, e.getMessage(), "Error!", ERROR_MESSAGE);
					}
				} while (true);
			}
		});
		return true;
	}

	private void load(int returnValue) throws Exception {
		if (returnValue == APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			try {
				doLoad(file);
			} catch (JsonParseException e) {
				throw new Exception("Malformed file!");
			} catch (JsonMappingException e) {
				throw new Exception("Deserialization problem! " + e.getMessage());
			} catch (MalformedURLException e) {
				throw new Exception("Malformed file location!");
			} catch (IOException e) {
				throw new Exception("IO Exception! " + e.getMessage());
			}
		}
	}

	private void doLoad(File file) throws JsonParseException, JsonMappingException, MalformedURLException, IOException, DuplicateException {
		int currentNumberOfBodies = system.getBodies().size();
		system.loadData(file.toURI().toURL());
		int newNumberOfBodies = system.getBodies().size() - currentNumberOfBodies;
		showMessageDialog(null, format("Data loaded correctly! A %d new body(s) appeared!", newNumberOfBodies), "Data was sucessfully read!",
				INFORMATION_MESSAGE);
	}
}