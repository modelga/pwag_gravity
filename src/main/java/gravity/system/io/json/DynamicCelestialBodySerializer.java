package gravity.system.io.json;

import gravity.physics.DynamicCelestialBody;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DynamicCelestialBodySerializer extends JsonSerializer<DynamicCelestialBody> {

	@Override
	public void serialize(DynamicCelestialBody value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeObjectField("position", value.getAbsolutePosition());
		jgen.writeObjectField("body", value.getBody());
		jgen.writeObjectField("velocityVector", value.getVelocityVector());
		jgen.writeObjectField("isSun", value.isSun());
		jgen.writeEndObject();
	}

}
