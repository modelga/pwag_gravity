package gravity.system.io.json;

import gravity.physics.BodyStaticData;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class BodyStaticDataSerializer extends AmountSerializer<BodyStaticData> {

	@Override
	public void serialize(BodyStaticData value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeObjectField("name", value.getName());
		jgen.writeObjectField("mass", serializeAmount(value.getMass()));
		jgen.writeObjectField("radius", serializeAmount(value.getRadius()));
		jgen.writeEndObject();
	}

}
