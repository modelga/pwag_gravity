package gravity.system.io.json;

import gravity.physics.util.Vector3;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class Vector3Serializer extends AmountSerializer<Vector3> {

	@Override
	public void serialize(Vector3 value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeObjectField("x", serializeAmount(value.getX()));
		jgen.writeObjectField("y", serializeAmount(value.getY()));
		jgen.writeObjectField("z", serializeAmount(value.getZ()));
		jgen.writeEndObject();
	}

}
