package gravity.system.io.json;

import javax.measure.quantity.Quantity;

import org.jscience.physics.amount.Amount;

import com.fasterxml.jackson.databind.JsonSerializer;

abstract public class AmountSerializer<T> extends JsonSerializer<T> {
	protected String serializeAmount(Amount<? extends Quantity> amount) {
		return amount.getEstimatedValue() + amount.getUnit().toString();
	}
}
