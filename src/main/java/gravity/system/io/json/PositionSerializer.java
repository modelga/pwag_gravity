package gravity.system.io.json;

import gravity.physics.util.Position;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class PositionSerializer extends AmountSerializer<Position> {

	@Override
	public void serialize(Position value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeObjectField("x", serializeAmount(value.getX()));
		jgen.writeObjectField("y", serializeAmount(value.getY()));
		jgen.writeObjectField("z", serializeAmount(value.getZ()));
		jgen.writeEndObject();
	}

}
