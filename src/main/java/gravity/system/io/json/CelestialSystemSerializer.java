package gravity.system.io.json;

import gravity.physics.DynamicCelestialBody;
import gravity.system.manager.CelestialSystem;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CelestialSystemSerializer extends JsonSerializer<CelestialSystem> {

	@Override
	public void serialize(CelestialSystem value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartArray();
		for (DynamicCelestialBody body : value.getBodies()) {
			jgen.writeObject(body);
		}
		jgen.writeEndArray();
	}

}
