package gravity.system.io.json;

import gravity.physics.util.ForcedVector3;

import java.io.IOException;

import javax.measure.quantity.Quantity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;

public class ForcedVector3Serializer extends AmountSerializer<ForcedVector3<Quantity>> {

	@Override
	public void serialize(ForcedVector3<Quantity> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
		jgen.writeStartObject();
		jgen.writeObjectField("jointPoint", value.getJointPosition());
		jgen.writeObjectField("direction", value.getDirection());
		jgen.writeObjectField("power", serializeAmount(value.getPower()));
		jgen.writeEndObject();
	}

}
