package gravity.system.api;

public class DuplicateException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8794200005281434170L;

	public DuplicateException(String string) {
		super(string);
	}

}
