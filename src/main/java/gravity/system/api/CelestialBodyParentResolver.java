package gravity.system.api;

import gravity.physics.DynamicCelestialBody;

public interface CelestialBodyParentResolver {
	DynamicCelestialBody resolveBody(String name) throws NotFoundException;
}
