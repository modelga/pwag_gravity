package gravity.system.api;

import gravity.physics.DynamicCelestialBody;

import java.util.Collection;

public interface CelestialBodyLifeCycleListener {

	void addBody(DynamicCelestialBody body);

	void addBody(Collection<DynamicCelestialBody> bodies);

	void removeBody(DynamicCelestialBody body);

	void removeAll();
}
