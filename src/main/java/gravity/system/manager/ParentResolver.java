package gravity.system.manager;

import gravity.physics.DynamicCelestialBody;
import gravity.system.api.CelestialBodyLifeCycleListener;
import gravity.system.api.CelestialBodyParentResolver;
import gravity.system.api.NotFoundException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ParentResolver implements CelestialBodyLifeCycleListener, CelestialBodyParentResolver {

	private final Map<String, DynamicCelestialBody> map = new HashMap<String, DynamicCelestialBody>();

	public DynamicCelestialBody resolveBody(String name) throws NotFoundException {
		if (map.containsKey(name)) {
			return map.get(name);
		}
		throw new NotFoundException();
	}

	public void addData(Collection<DynamicCelestialBody> bodies) {
		addBody(bodies);
	}

	public void addBody(Collection<DynamicCelestialBody> bodies) {
		for (DynamicCelestialBody body : bodies) {
			addBody(body);
		}
	}

	public void addBody(DynamicCelestialBody body) {
		String name = body.getBody().getName();
		if (map.containsKey(name)) {
			throw new RuntimeException("Error celestial body named `" + name + "` exists!");
		}
		map.put(body.getBody().getName(), body);
	}

	public void removeBody(DynamicCelestialBody body) {
		throw new UnsupportedOperationException("Remove single body is not supported");
	}

	public void removeAll() {
		map.clear();
	}

}
