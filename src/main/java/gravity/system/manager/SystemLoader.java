package gravity.system.manager;

import gravity.physics.DynamicCelestialBody;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SystemLoader {

	private final TypeReference<Collection<DynamicCelestialBody>> collectionType;

	public SystemLoader() {
		collectionType = new TypeReference<Collection<DynamicCelestialBody>>() {
		};
	}

	public Collection<DynamicCelestialBody> load(URL url) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(url, collectionType);
	}

}
