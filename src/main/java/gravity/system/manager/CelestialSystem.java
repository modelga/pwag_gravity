package gravity.system.manager;

import static java.lang.String.format;
import gravity.physics.DynamicCelestialBody;
import gravity.physics.api.PhysicsUpdater;
import gravity.system.api.CelestialBodyLifeCycleListener;
import gravity.system.api.CelestialBodyParentResolver;
import gravity.system.api.DuplicateException;
import gravity.system.api.NotFoundException;
import gravity.system.io.json.CelestialSystemSerializer;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = CelestialSystemSerializer.class)
public class CelestialSystem {
	private final Collection<DynamicCelestialBody> bodies;
	private final SystemLoader loader;
	private final Collection<CelestialBodyLifeCycleListener> lifeCycleListeners;
	private final Collection<CelestialBodyParentResolver> parentResolvers;
	private final PhysicsUpdater physicsUpdater;
	private boolean isPaused = false;
	private boolean isSimulationResumed = true;

	public CelestialSystem(SystemLoader loader, PhysicsUpdater physicsUpdater) {
		this.loader = loader;
		this.physicsUpdater = physicsUpdater;
		this.bodies = new ArrayList<DynamicCelestialBody>();
		this.lifeCycleListeners = new ArrayList<CelestialBodyLifeCycleListener>();
		this.parentResolvers = new ArrayList<CelestialBodyParentResolver>();
	}

	public void addListener(CelestialBodyLifeCycleListener listener) {
		lifeCycleListeners.add(listener);
	}

	public void addParentResolver(CelestialBodyParentResolver resolver) {
		parentResolvers.add(resolver);
	}

	public void tryToResolve() {
		for (DynamicCelestialBody body : bodies) {
			if (!body.isParentResolved()) {
				tryToResolve(body);
			}
		}
	}

	public void waitForEnd(Runnable task) {
		this.isPaused = true;
		this.isSimulationResumed = false;
		task.run();
		this.isPaused = false;
	}

	private void tryToResolve(DynamicCelestialBody body) {
		for (CelestialBodyParentResolver resolver : parentResolvers) {
			try {
				DynamicCelestialBody resolvedBody = resolver.resolveBody(body.getParentToResolve());
				body.setParent(resolvedBody);
			} catch (NotFoundException e) {
				System.out.println(format("Cannot resolve parent named `%s` to %s", body.getParentToResolve(), body.getBody().getName()));
			}
		}
	}

	public void loadData(URL url) throws JsonParseException, JsonMappingException, IOException, DuplicateException {
		addBodies(loader.load(url));
	}

	public Collection<DynamicCelestialBody> getBodies() {
		return bodies;
	}

	public void update(double deltaT) {
		if (!isPaused) {
			if (!isSimulationResumed) {
				deltaT = 0;
				isSimulationResumed = true;
			}
			physicsUpdater.update(getBodies(), deltaT);
		}
	}

	public void addBodies(Collection<DynamicCelestialBody> addend) throws DuplicateException {

		DynamicCelestialBody[] temp = addend.toArray(new DynamicCelestialBody[0]);
		for (int i = 0; i < temp.length; i++) {
			for (int j = (i + 1); j < temp.length; j++) {
				if (temp[i].getBody().getName().equals(temp[j].getBody().getName())) {
					throw new DuplicateException("Error celestial body named `" + temp[i].getBody().getName() + "` exists!");
				}
			}
		}

		for (DynamicCelestialBody body : bodies) {
			for (DynamicCelestialBody addnedBody : addend) {
				if (body.getBody().getName().equals(addnedBody.getBody().getName())) {
					throw new DuplicateException("Error celestial body named `" + body.getBody().getName() + "` exists!");
				}
			}
		}

		for (DynamicCelestialBody dynamicCelestialBody : addend) {
			addBody(dynamicCelestialBody);
		}
	}

	public void addBody(DynamicCelestialBody addend) throws DuplicateException {
		checkForDuplicates(addend);
		bodies.add(addend);
		notifyListeners(addend);
		tryToResolve();
	}

	private void checkForDuplicates(DynamicCelestialBody addend) throws DuplicateException {
		String name = addend.getBody().getName();
		for (DynamicCelestialBody body : bodies) {
			if (name.equals(body.getBody().getName())) {
				throw new DuplicateException("Error celestial body named `" + name + "` exists!");
			}
		}
	}

	private void notifyListeners(DynamicCelestialBody addend) {
		for (CelestialBodyLifeCycleListener listener : lifeCycleListeners) {
			listener.addBody(addend);
		}
	}

	public String toJSON() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writer().withDefaultPrettyPrinter().writeValueAsString(this);
		// return mapper.writeValueAsString(this);
	}

	public void removeAll() {
		bodies.clear();
		for (CelestialBodyLifeCycleListener listener : lifeCycleListeners) {
			listener.removeAll();
		}
	}
}
