package gravity.physics;

import gravity.physics.util.ForcedVector3;
import gravity.physics.util.Position;
import gravity.physics.util.Vector3;
import gravity.system.io.json.DynamicCelestialBodySerializer;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Force;
import javax.measure.quantity.Length;
import javax.measure.quantity.Velocity;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;
import org.jscience.physics.amount.Constants;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = DynamicCelestialBodySerializer.class)
public class DynamicCelestialBody {

	private DynamicCelestialBody parent;
	private final BodyStaticData body;
	private Position position = null;
	private ForcedVector3<Velocity> velocityVector;
	private ForcedVector3<Force> currentForce;
	private String parentToResolve;
	private boolean isSun;

	public DynamicCelestialBody(BodyStaticData body, Position position) {
		this(body, position, null, null, true);
	}

	@JsonCreator
	public DynamicCelestialBody(@JsonProperty("body") BodyStaticData body, @JsonProperty("position") Position position,
			@JsonProperty("parent") String parentToResolve, @JsonProperty("velocityVector") ForcedVector3<Velocity> velocityVector,
			@JsonProperty("isSun") boolean isSun) {
		this.parentToResolve = parentToResolve;
		this.body = body;
		this.position = position;
		this.isSun = isSun;
		if (velocityVector != null) {
			this.velocityVector = velocityVector;
		} else {
			this.velocityVector = new ForcedVector3<Velocity>(getPosition(), new Vector3(), Amount.valueOf(0, SI.METERS_PER_SECOND));
		}
	}

	public DynamicCelestialBody(BodyStaticData body, Position position, DynamicCelestialBody parent) {
		this.parent = parent;
		this.isSun = (this.parent == null);
		this.body = body;
		this.position = position;
		this.parentToResolve = parent.getBody().getName();
		calculateVelocityForCircularOrbit();
	}

	public boolean isParentResolved() {
		return parent != null || parentToResolve == null || parentToResolve.isEmpty();
	}

	public void setParent(DynamicCelestialBody parent) {
		this.parent = parent;
		this.isSun = (this.parent == null);
		this.parentToResolve = null;
		calculateVelocityForCircularOrbit();
	}

	public Position getRelativePosition() {
		return position;
	};

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@SuppressWarnings("unused")
	private void addPosition(Position addend) {
		setPosition(this.position.add(addend));
	}

	public DynamicCelestialBody getParent() {
		return parent;
	}

	public String getParentToResolve() {
		return parentToResolve;
	}

	public Position getAbsolutePosition() {
		Position absolutePosition = getRelativePosition();

		for (DynamicCelestialBody currentParent = parent; currentParent != null; currentParent = currentParent.parent) {
			absolutePosition = absolutePosition.add(currentParent.position);
		}

		return absolutePosition;
	}

	public BodyStaticData getBody() {
		return body;
	}

	@Override
	public String toString() {
		return String.format("%s", getBody().getName());
	}

	public Amount<Velocity> calculateVelocityPowerForCircularOrbit() {
		//Amount<?> G = Constants.G;
		double radius = new Vector3(getAbsolutePosition(), parent.getAbsolutePosition()).getLength().getEstimatedValue();
		if (radius <= 0) {
			return Amount.valueOf(0, SI.METERS_PER_SECOND);
		}
		double velocityPower =Math.sqrt(( parent.getBody().getMass().getEstimatedValue() * Constants.G.getEstimatedValue() )/radius);
		return Amount.valueOf(velocityPower, SI.METERS_PER_SECOND);
	}

	@SuppressWarnings("unchecked")
	private Vector3 calculateVelocityDirectonForCircularOrbit() {
		Amount<Length> meter = Amount.valueOf(1, SI.METER);
		Vector3 toParentDirection = new Vector3(getAbsolutePosition(), getParent().getAbsolutePosition());

		/*
		 * plane equation Ax+By+Cz+D=0 : tangent plane to sphere with center in
		 * parent position and radius equal length from pareny to child [A,B,C]
		 * is toParentDirection D is planeConstant calculate from equation where
		 * [x,y,z] is child position, because child is on plane
		 */
		Amount<Dimensionless> planeConstant = (Amount<Dimensionless>) toParentDirection.getX().times(getAbsolutePosition().getX().divide(meter))
				.plus(toParentDirection.getY().times(getAbsolutePosition().getY().divide(meter)))
				.plus(toParentDirection.getZ().times(getAbsolutePosition().getZ().divide(meter))).times(-1);
		Position randomPoint;
		Amount<Dimensionless> t = Amount.ZERO;
		Amount<Length> x;
		Amount<Length> y;
		Amount<Length> z;
		Position newPoint;
		do {
			randomPoint = Position.random();
			t = t.minus(randomPoint.getX().divide(meter).times(toParentDirection.getX()));
			t = t.minus(randomPoint.getY().divide(meter).times(toParentDirection.getY()));
			t = t.minus(randomPoint.getZ().divide(meter).times(toParentDirection.getZ()));
			t = t.minus(planeConstant);
			t = (Amount<Dimensionless>) t.divide(toParentDirection.getX().pow(2).plus(toParentDirection.getY().pow(2).plus(toParentDirection.getZ().pow(2))));
			x = randomPoint.getX().plus(t.times(toParentDirection.getX().times(meter)));
			y = randomPoint.getY().plus(t.times(toParentDirection.getY().times(meter)));
			z = randomPoint.getZ().plus(t.times(toParentDirection.getZ().times(meter)));
			newPoint = new Position(x, y, z);
		} while (newPoint.equals(getPosition()));
		return new Vector3(getPosition(), newPoint);
	}

	private void calculateVelocityForCircularOrbit() {
		if (parent == null) {
			velocityVector = new ForcedVector3<Velocity>(getPosition(), new Vector3(), Amount.valueOf(0, SI.METERS_PER_SECOND));
		} else {
			velocityVector = new ForcedVector3<Velocity>(getPosition(), calculateVelocityDirectonForCircularOrbit(), calculateVelocityPowerForCircularOrbit());
			// velocityVector.add(new
			// ForcedVector3<Velocity>(parent.getVelocityVector()));
		}
	}

	public ForcedVector3<Velocity> getVelocityVector() {
		return velocityVector;
	}

	public ForcedVector3<Force> getForceVector() {
		return currentForce;
	}

	public boolean isCollisionWith(DynamicCelestialBody secondBody) {
		double distance = new Vector3(getAbsolutePosition(), secondBody.getAbsolutePosition()).getLength().getEstimatedValue();
		double radiusSum = this.body.getRadius().getEstimatedValue() + secondBody.body.getRadius().getEstimatedValue();
		if (distance < radiusSum) {
			return true;
		}
		return false;
	}

	public void resetForce() {
		currentForce = new ForcedVector3<Force>(getPosition(), new Vector3(), Amount.valueOf(0, SI.NEWTON));
	}

	public void addForce(DynamicCelestialBody secondBody) {
		Position a = getAbsolutePosition();
		Position b = secondBody.getAbsolutePosition();
		Position c = new Position(b.getX().getEstimatedValue() - a.getX().getEstimatedValue(), b.getY().getEstimatedValue() - a.getY().getEstimatedValue(), b
				.getZ().getEstimatedValue() - a.getZ().getEstimatedValue());
		Vector3 forceDierction = new Vector3(c).normalize();
		double distanceSquare = new Vector3(c).getLength().getEstimatedValue();
		distanceSquare *= distanceSquare;
		double massTimesMass = body.getMass().getEstimatedValue() * secondBody.body.getMass().getEstimatedValue();
		double gravityPower = (massTimesMass * Constants.G.getEstimatedValue()) / distanceSquare;
		ForcedVector3<Force> temp = new ForcedVector3<Force>(getPosition(), forceDierction, Amount.valueOf(gravityPower, SI.NEWTON));
		// System.out.println(temp.toString());
		currentForce = currentForce.add(temp);
	}

	public void updateVelocity(double dt) {
		@SuppressWarnings("unchecked")
		Amount<Dimensionless> dtToMass = (Amount<Dimensionless>) Amount.ONE.times(dt).divide(body.getMass().divide(Amount.valueOf(1, SI.KILOGRAM)));
		if (currentForce.getDirection().isZero()) {
			return;
		}
		@SuppressWarnings("unchecked")
		ForcedVector3<Velocity> deltaVelocity = new ForcedVector3<Velocity>(getAbsolutePosition(), currentForce.getDirection(), (Amount<Velocity>) currentForce
				.getPowerFactor().times(Amount.valueOf(1, SI.METERS_PER_SECOND)));
		deltaVelocity = deltaVelocity.times(dtToMass);
		velocityVector = velocityVector.add(deltaVelocity);
	}

	public void updatePosition(double dt) {
		double velocityPowerTimesDt = velocityVector.getPower().getEstimatedValue() * dt;
		double deltaX = velocityVector.getDirection().getX().getEstimatedValue() * velocityPowerTimesDt;
		double deltaY = velocityVector.getDirection().getY().getEstimatedValue() * velocityPowerTimesDt;
		double deltaZ = velocityVector.getDirection().getZ().getEstimatedValue() * velocityPowerTimesDt;
		Position deltaPosition = new Position(deltaX, deltaY, deltaZ);
		position = position.add(deltaPosition);
	}

	public boolean isSun() {
		return isSun;
	}

	public void setSun(boolean isSun) {
		this.isSun = isSun;
	}

}
