package gravity.physics.api;

import gravity.physics.DynamicCelestialBody;

import java.util.Collection;

public interface PhysicsUpdater {
	void update(Collection<DynamicCelestialBody> bodies, double deltaT);
}
