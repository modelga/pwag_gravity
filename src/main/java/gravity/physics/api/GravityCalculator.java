package gravity.physics.api;

import gravity.physics.DynamicCelestialBody;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Force;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

public interface GravityCalculator {

	static public final Amount<Length> AU = Amount.valueOf(149597887, SI.KILOMETER);

	Amount<Force> calculateGravity(DynamicCelestialBody source, DynamicCelestialBody target);

	Amount<Acceleration> calculateAccelerationAtSurface();

	Amount<Acceleration> calculateAccelerationAboveSurface(Amount<Length> above) throws IllegalArgumentException;
}
