package gravity.physics.api;

public interface XYZ<T> {
	T getX();

	T getY();

	T getZ();

}
