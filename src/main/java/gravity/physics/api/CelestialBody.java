package gravity.physics.api;

import javax.measure.quantity.Length;
import javax.measure.quantity.Mass;

import org.jscience.physics.amount.Amount;

public interface CelestialBody {
	public Amount<Length> getRadius();

	public int getId();

	public String getName();

	public Amount<Mass> getMass();
}
