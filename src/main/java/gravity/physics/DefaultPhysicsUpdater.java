package gravity.physics;

import gravity.physics.api.PhysicsUpdater;

import java.util.Collection;

public class DefaultPhysicsUpdater implements PhysicsUpdater {

	public void update(Collection<DynamicCelestialBody> bodies, double deltaT) {
		if (deltaT > 0) {
			calculateForces(bodies);
			calculateVelocities(bodies, deltaT);
			updatePositions(bodies, deltaT);
		}
	}

	private void calculateForces(Collection<DynamicCelestialBody> bodies) {
		for (DynamicCelestialBody current : bodies) {
			current.resetForce();
			for (DynamicCelestialBody influenceFrom : bodies) {
				if (!current.equals(influenceFrom) && !current.isCollisionWith(influenceFrom)) {
					current.addForce(influenceFrom);
				}
			}
		}
	}

	private void calculateVelocities(Collection<DynamicCelestialBody> bodies, double dt) {
		for (DynamicCelestialBody current : bodies) {
			current.updateVelocity(dt);
		}
	}

	private void updatePositions(Collection<DynamicCelestialBody> bodies, double dt) {
		for (DynamicCelestialBody current : bodies) {
			current.updatePosition(dt);
		}
	}

}
