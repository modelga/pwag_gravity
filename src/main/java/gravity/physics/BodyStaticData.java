package gravity.physics;

import gravity.physics.api.CelestialBody;
import gravity.physics.util.CelestialBodyIdGenerator;
import gravity.system.io.json.BodyStaticDataSerializer;

import javax.measure.quantity.Length;
import javax.measure.quantity.Mass;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = BodyStaticDataSerializer.class)
public class BodyStaticData implements CelestialBody {

	private final int id;
	private final String name;
	private final Amount<Mass> mass;
	private final Amount<Length> radius;

	public BodyStaticData(String name, Amount<Mass> mass, Amount<Length> radius) {
		this.id = CelestialBodyIdGenerator.nextId();
		this.name = name;
		this.mass = mass;
		this.radius = radius;
	}

	@JsonCreator
	public BodyStaticData(@JsonProperty("name") String name, @JsonProperty("mass") String mass, @JsonProperty("radius") String radius) {
		this(name, Amount.valueOf(mass).to(SI.KILOGRAM), Amount.valueOf(radius).to(SI.METER));
	}

	protected BodyStaticData(BodyStaticData clone) {
		this.id = CelestialBodyIdGenerator.nextId();
		this.name = clone.getName();
		this.mass = clone.getMass();
		this.radius = clone.getRadius();
	}

	public Amount<Length> getRadius() {
		return radius;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Amount<Mass> getMass() {
		return mass;
	}

	@Override
	public String toString() {
		return String.format("[Name: %s (%d), mass: %s, radius: %s]", name, id, mass.toString(), radius.toString());
	}
}
