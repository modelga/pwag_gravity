package gravity.physics.util;

public final class CelestialBodyIdGenerator {

	private static CelestialBodyIdGenerator instance;

	private int id = 1;

	private CelestialBodyIdGenerator() {

	}

	private int generateNextId() {
		return id++;
	}

	public static int nextId() {
		if (instance == null) {
			instance = new CelestialBodyIdGenerator();
		}
		return instance.generateNextId();
	}

}
