package gravity.physics.util;

import static org.jscience.physics.amount.Amount.ONE;
import static org.jscience.physics.amount.Amount.ZERO;
import gravity.physics.api.XYZ;
import gravity.system.io.json.Vector3Serializer;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Quantity;

import org.jscience.physics.amount.Amount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = Vector3Serializer.class)
public class Vector3 implements XYZ<Amount<? extends Quantity>> {

	private final Amount<Dimensionless> x;
	private final Amount<Dimensionless> y;
	private final Amount<Dimensionless> z;

	public Vector3() {
		y = z = x = ZERO;
	}

	public Vector3(Amount<Dimensionless> x, Amount<Dimensionless> y, Amount<Dimensionless> z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	@SuppressWarnings("unchecked")
	public Vector3(Vector3 vec) {
		this.x = (Amount<Dimensionless>) Amount.ONE.times(vec.x);
		this.y = (Amount<Dimensionless>) Amount.ONE.times(vec.y);
		this.z = (Amount<Dimensionless>) Amount.ONE.times(vec.z);
	}

	@JsonCreator
	public Vector3(@JsonProperty("x") String x, @JsonProperty("y") String y, @JsonProperty("z") String z) {
		this(Double.valueOf(x), Double.valueOf(y), Double.valueOf(z));
	}

	public Vector3(double x, double y, double z) {
		this.x = ONE.times(x);
		this.y = ONE.times(y);
		this.z = ONE.times(z);
	}

	public Vector3(Position from, Position to) {
		this(to.minus(from));
	}

	public Vector3(Position vector) {
		this.x = getDimensionlessValue(vector.getX());
		this.y = getDimensionlessValue(vector.getY());
		this.z = getDimensionlessValue(vector.getZ());
	}

	public Amount<Dimensionless> getLength() {

		double xSquare = x.getEstimatedValue() * x.getEstimatedValue();
		double ySquare = y.getEstimatedValue() * y.getEstimatedValue();
		double zSquare = z.getEstimatedValue() * z.getEstimatedValue();
		double lengthSquare = xSquare + ySquare + zSquare;
		if (lengthSquare == 0) {
			return ZERO;
		} else {
			return ONE.times(Math.sqrt(lengthSquare));

		}
	}

	public Vector3 normalize() {
		double maxAbsValueInCoordinates = Math.abs(x.getEstimatedValue());
		if (Math.abs(y.getEstimatedValue()) > maxAbsValueInCoordinates) {
			maxAbsValueInCoordinates = Math.abs(y.getEstimatedValue());
		}
		if (Math.abs(z.getEstimatedValue()) > maxAbsValueInCoordinates) {
			maxAbsValueInCoordinates = Math.abs(z.getEstimatedValue());
		}

		Vector3 tempVector;

		if (maxAbsValueInCoordinates > 1) {
			double tempX = x.getEstimatedValue() / maxAbsValueInCoordinates;
			double tempY = y.getEstimatedValue() / maxAbsValueInCoordinates;
			double tempZ = z.getEstimatedValue() / maxAbsValueInCoordinates;
			tempVector = new Vector3(tempX, tempY, tempZ);
		} else {
			tempVector = new Vector3(x.getEstimatedValue(), y.getEstimatedValue(), z.getEstimatedValue());
		}

		double length = tempVector.getLength().getEstimatedValue();
		if (length == 0) {
			return new Vector3();
		}
		double newX = tempVector.x.getEstimatedValue() / length;
		double newY = tempVector.y.getEstimatedValue() / length;
		double newZ = tempVector.z.getEstimatedValue() / length;
		boolean needSecondNormalize = false;

		if (newX > 1) {
			newX = 1;
			needSecondNormalize = true;
		} else if (newX < -1) {
			newX = -1;
			needSecondNormalize = true;
		}

		if (newY > 1) {
			newY = 1;
			needSecondNormalize = true;
		} else if (newY < -1) {
			newY = -1;
			needSecondNormalize = true;
		}

		if (newZ > 1) {
			newZ = 1;
			needSecondNormalize = true;
		} else if (newZ < -1) {
			newZ = -1;
			needSecondNormalize = true;
		}

		Vector3 result = new Vector3(newX, newY, newZ);

		if (needSecondNormalize) {
			result = result.normalize();
		}

		return result;
	}

	protected static Amount<Dimensionless> getDimensionlessValue(Amount<? extends Quantity> length) {
		return ONE.times(length.getEstimatedValue());
	}

	public Vector3 times(Amount<? extends Quantity> value) {
		return Vector3.times(this, getDimensionlessValue(value));
	}

	public static Vector3 times(Vector3 vector, Amount<Dimensionless> value) {
		double productX = vector.getX().getEstimatedValue() * value.getEstimatedValue();
		double productY = vector.getY().getEstimatedValue() * value.getEstimatedValue();
		double productZ = vector.getZ().getEstimatedValue() * value.getEstimatedValue();
		return new Vector3(productX, productY, productZ);
	}

	public Vector3 add(Vector3 addend) {
		double productX = addend.getX().getEstimatedValue() + this.x.getEstimatedValue();
		double productY = addend.getY().getEstimatedValue() + this.y.getEstimatedValue();
		double productZ = addend.getZ().getEstimatedValue() + this.z.getEstimatedValue();
		return new Vector3(productX, productY, productZ);
	}

	@Override
	public String toString() {
		return String.format("[x: %s, y: %s, z:%s]", getX(), getY(), getZ());
	}

	public static Vector3 random() {
		return new Vector3(ONE.times(Math.random() * 2 - 1), ONE.times(Math.random() * 2 - 1), ONE.times(Math.random() * 2 - 1));
	}

	@Override
	public Amount<Dimensionless> getX() {
		return x;
	}

	@Override
	public Amount<Dimensionless> getY() {
		return y;
	}

	@Override
	public Amount<Dimensionless> getZ() {
		return z;
	}

	public boolean isZero() {
		if (x.approximates(ZERO) && y.approximates(ZERO) && z.approximates(ZERO)) {
			return true;
		}
		return false;
	}
}
