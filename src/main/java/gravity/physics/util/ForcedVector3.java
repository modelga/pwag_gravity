package gravity.physics.util;

import static gravity.physics.util.Vector3.getDimensionlessValue;
import gravity.physics.api.XYZ;
import gravity.system.io.json.ForcedVector3Serializer;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Quantity;

import org.jscience.physics.amount.Amount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = ForcedVector3Serializer.class)
public class ForcedVector3<PowerType extends Quantity> implements XYZ<Amount<? extends Quantity>> {
	private Amount<PowerType> power;
	private final Position jointPoint;
	private final Vector3 directionVector;

	public ForcedVector3(Position jointPoint, Position targetPoint, Amount<PowerType> power) {
		this.jointPoint = jointPoint;
		this.directionVector = new Vector3(jointPoint, targetPoint).normalize();
		this.power = power;
	}

	@SuppressWarnings("unchecked")
	public ForcedVector3(Position jointPoint, Vector3 direction, Amount<PowerType> power) {
		this.jointPoint = new Position(jointPoint);
		this.directionVector = new Vector3(direction.normalize());
		this.power = ((Amount<PowerType>) Amount.valueOf(power.getEstimatedValue(), power.getUnit().getStandardUnit()));
	}

	@SuppressWarnings("unchecked")
	@JsonCreator
	public ForcedVector3(@JsonProperty("jointPoint") Position jointPoint, @JsonProperty("direction") Vector3 direction, @JsonProperty("power") String power) {
		this(jointPoint, direction, (Amount<PowerType>) Amount.valueOf(power));
	}

	public ForcedVector3(ForcedVector3<PowerType> velocityVector) {
		this(velocityVector.jointPoint, velocityVector.directionVector, velocityVector.power);
	}

	@SuppressWarnings("unchecked")
	public ForcedVector3<PowerType> add(ForcedVector3<PowerType> addend) {
		Vector3 a = this.getDirection().times(getPowerFactor());
		Vector3 b;
		if (addend != null) {
			b = addend.getDirection().times(addend.getPowerFactor());
		} else {
			b = new Vector3(0, 0, 0);
		}
		Vector3 result = a.add(b);
		Amount<Dimensionless> dimensionlessValue = (result.getLength());
		Amount<PowerType> newValue = Amount.valueOf(1, power.getUnit());
		newValue = (Amount<PowerType>) newValue.times(dimensionlessValue);
		return new ForcedVector3<PowerType>(jointPoint, result, newValue);
	}

	public Amount<Dimensionless> getPowerFactor() {
		return getDimensionlessValue(this.getPower());
	}

	public Amount<PowerType> getPower() {
		return power;
	}

	public Position getJointPosition() {
		return jointPoint;
	}

	public ForcedVector3<PowerType> times(Amount<Dimensionless> value) {
		Amount<PowerType> tempPower = getPower().times(value.getEstimatedValue());
		ForcedVector3<PowerType> result = new ForcedVector3<PowerType>(jointPoint, getDirection(), tempPower);
		return result;
	}

	public Vector3 getDirection() {
		return directionVector.normalize();
	}

	@Override
	public String toString() {
		return super.toString() + String.format("[direction: %s, power: %s, joint: %s]", directionVector, power, jointPoint);
	}

	public Amount<Dimensionless> getX() {
		return directionVector.normalize().getX();
	}

	public Amount<Dimensionless> getY() {
		return directionVector.normalize().getY();
	}

	public Amount<Dimensionless> getZ() {
		return directionVector.normalize().getZ();
	}
}
