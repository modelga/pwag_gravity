package gravity.physics.util;

import gravity.physics.api.XYZ;
import gravity.system.io.json.PositionSerializer;

import javax.measure.quantity.Length;
import javax.measure.quantity.Quantity;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = PositionSerializer.class)
public class Position implements XYZ<Amount<? extends Quantity>> {
	private final Amount<Length> x;
	private final Amount<Length> y;
	private final Amount<Length> z;

	public Position() {
		x = Amount.valueOf(0, SI.METER);
		y = Amount.valueOf(0, SI.METER);
		z = Amount.valueOf(0, SI.METER);
	}

	@JsonCreator
	public Position(@JsonProperty("x") String x, @JsonProperty("y") String y, @JsonProperty("z") String z) {
		this(Amount.valueOf(x).to(SI.METER), Amount.valueOf(y).to(SI.METER), Amount.valueOf(z).to(SI.METER));
	}

	@SuppressWarnings("unchecked")
	public Position(Amount<? extends Quantity> x, Amount<? extends Quantity> y, Amount<? extends Quantity> z) {
		this.x = (Amount<Length>) x;
		this.y = (Amount<Length>) y;
		this.z = (Amount<Length>) z;
	}

	public Position(float x, float y, float z) {
		this.x = Amount.valueOf(x, SI.METER);
		this.y = Amount.valueOf(y, SI.METER);
		this.z = Amount.valueOf(z, SI.METER);
	}

	public Position(double x, double y, double z) {
		this.x = Amount.valueOf(x, SI.METER);
		this.y = Amount.valueOf(y, SI.METER);
		this.z = Amount.valueOf(z, SI.METER);
	}

	public Position(Position pos) {
		this.x = pos.x;
		this.y = pos.y;
		this.z = pos.z;
	}

	public Amount<Length> getX() {
		return x;
	}

	public Amount<Length> getY() {
		return y;
	}

	public Amount<Length> getZ() {
		return z;
	}

	public Position add(Position addend) {
		Amount<Length> newX = this.x.plus(addend.getX());
		Amount<Length> newY = this.y.plus(addend.getY());
		Amount<Length> newZ = this.z.plus(addend.getZ());
		return new Position(newX, newY, newZ);
	}

	public Position minus(Position substract) {
		Amount<Length> newX = this.getX().minus(substract.getX());
		Amount<Length> newY = this.getY().minus(substract.getY());
		Amount<Length> newZ = this.getZ().minus(substract.getZ());
		return new Position(newX, newY, newZ);
	}

	public boolean isApproximate(Position p) {
		return isApproximate(getX(), p.getX()) && isApproximate(getY(), p.getY()) && isApproximate(getZ(), p.getZ());
	}

	private boolean isApproximate(Amount<Length> aThis, Amount<Length> that) {
		return aThis.approximates(that);
	}

	@Override
	public String toString() {
		return String.format("[x: %s, y: %s, z: %s]", x, y, z);
	}

	public static Position random() {
		return new Position(Amount.valueOf(Math.random() * 2 - 1, SI.METER), Amount.valueOf(Math.random() * 2 - 1, SI.METER), Amount.valueOf(
				Math.random() * 2 - 1, SI.METER));
	}

	@Override
	public boolean equals(Object obj) {
		if (!(this.getClass().equals(obj.getClass()))) {
			return false;
		}
		Position pos = (Position) obj;
		if (!this.getX().approximates(pos.getX())) {
			return false;
		}
		if (!this.getY().approximates(pos.getY())) {
			return false;
		}
		if (!this.getZ().approximates(pos.getZ())) {
			return false;
		}
		return true;
	}
}
