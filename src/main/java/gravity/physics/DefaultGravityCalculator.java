package gravity.physics;

import gravity.physics.api.GravityCalculator;
import gravity.physics.util.Vector3;

import javax.measure.quantity.Acceleration;
import javax.measure.quantity.Force;
import javax.measure.quantity.Length;
import javax.measure.unit.SI;

import org.jscience.physics.amount.Amount;
import org.jscience.physics.amount.Constants;

public class DefaultGravityCalculator implements GravityCalculator {

	public Amount<Force> calculateGravity(DynamicCelestialBody source, DynamicCelestialBody target) {
		BodyStaticData sourceBody = source.getBody();
		BodyStaticData targetBody = target.getBody();
		//Amount<Dimensionless> length = new Vector3(target.getAbsolutePosition(), source.getAbsolutePosition()).getLength();
		Amount<?> squaredDistance = new Vector3(target.getAbsolutePosition(), source.getAbsolutePosition()).getLength().pow(2);
		Amount<?> squaredMass = sourceBody.getMass().times(targetBody.getMass());
		Amount<Force> force = Constants.G.times(squaredMass).divide(squaredDistance).to(SI.NEWTON);
		return force;
	}

	public Amount<Acceleration> calculateAccelerationAtSurface() {
		// TOD Auto-generated method stub
		return null;
	}

	public Amount<Acceleration> calculateAccelerationAboveSurface(Amount<Length> above) throws IllegalArgumentException {
		// TOD Auto-generated method stub
		return null;
	}
}
