package gravity.physics;

import java.util.Collection;

public class PrecisePhysicsUpdater extends DefaultPhysicsUpdater {

	private double iterationTime;

	public PrecisePhysicsUpdater(double interationTime) {
		this.iterationTime = interationTime;
	}

	@Override
	public void update(Collection<DynamicCelestialBody> bodies, double deltaT) {
		if (deltaT < iterationTime) {
			super.update(bodies, deltaT);
		} else {
			long steps = (long) (deltaT / iterationTime);
			for (double step = 0; step < deltaT; step += iterationTime) {
				super.update(bodies, iterationTime);
			}
			double lackSimulationTime = deltaT - steps * iterationTime;
			super.update(bodies, lackSimulationTime);
		}
	}
}
