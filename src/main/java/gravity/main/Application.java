package gravity.main;

import gravity.config.VideoConfigData;
import gravity.visual.MainScreen;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Application {
	private static final String APPLICATION_TITLE = "Gravity";
	private static final boolean USE_GL30 = true;

	public Application(VideoConfigData config) {
		LwjglApplicationConfiguration lwjglConfig = new LwjglApplicationConfiguration();
		lwjglConfig.title = APPLICATION_TITLE;
		lwjglConfig.useGL30 = USE_GL30;
		lwjglConfig.width = config.getWidth();
		lwjglConfig.height = config.getHeight();
		lwjglConfig.depth = config.getBitDepth();
		lwjglConfig.fullscreen = false;
		lwjglConfig.resizable = false;
		new LwjglApplication(new MainScreen(), lwjglConfig);
	}
}