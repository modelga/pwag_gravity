package gravity.main;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {

		System.out.println("Application will be started...");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame window = new gravity.config.VideoConfigWindow();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		});

	}
}
