package gravity.main;

import gravity.config.VideoConfigData;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class DebugApplication {

	private final JFrame jFrame;

	public DebugApplication(VideoConfigData config) {
		jFrame = new JFrame();
		jFrame.setSize(config.getWidth(), config.getHeight());

		JButton jButton = new JButton();
		jButton.addActionListener(object);
		jButton.setText("Open new File");
		jFrame.add(jButton);
		jFrame.setVisible(true);
	}

	ActionListener object = new ActionListener() {

		public void actionPerformed(ActionEvent e) {
			JFileChooser fileChooser = new JFileChooser(new File("."));
			int openDialog = fileChooser.showOpenDialog(jFrame);
			if (openDialog == JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(jFrame, "Message");
			}

		}
	};
}
