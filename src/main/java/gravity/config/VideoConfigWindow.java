package gravity.config;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class VideoConfigWindow extends JFrame {

	private static final int MIN_HEIGHT = 768;
	private static final int MIN_WIDTH = 1024;
	private static final long serialVersionUID = -5577568662858147727L;
	private static final String TITLE = "Choose graphic mode...";
	private JPanel contentPane;
	JList<DisplayMode> displayModeList;
	JCheckBox debugMode;

	ActionListener runActionListener = new ApplicationStarter(this);

	public VideoConfigWindow() {
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 300, 340);
		setResizable(false);
		setLocationRelativeTo(null);
		createContent();
	}

	private void createContent() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		createVideoModePanel();
		createRunPanel();
	}

	private void createVideoModePanel() {
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(2, 1, 0, 0));
		JPanel videoModePanel = new JPanel();
		panel.add(videoModePanel);
		videoModePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		createVideoModeLabel(videoModePanel);
		createVideoModePane(videoModePanel);
		JPanel optionsPanel = new JPanel();
		addCheckBoxToPanel(optionsPanel, debugMode = new JCheckBox("Debug mode? (Soft Swing app)"));
		panel.add(optionsPanel);

	}

	private void addCheckBoxToPanel(JPanel panel, JCheckBox checkBox) {
		checkBox.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(checkBox);
	}

	private void createVideoModeLabel(JPanel videoModePanel) {
		JLabel lblVideoMode = new JLabel("Video Mode:");
		videoModePanel.add(lblVideoMode);
	}

	private void createVideoModePane(JPanel videoModePanel) {
		createDisplayModeList();
		JScrollPane scrollPane = new JScrollPane(displayModeList);
		scrollPane.setPreferredSize(new Dimension(250, 100));
		displayModeList.setSelectedIndex(0);
		videoModePanel.add(scrollPane);
	}

	private void createDisplayModeList() {
		GraphicsEnvironment enviroment = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = enviroment.getDefaultScreenDevice();
		displayModeList = new JList<DisplayMode>(filterDisplayModes(device.getDisplayModes(), MIN_WIDTH, MIN_HEIGHT));
		displayModeList.setCellRenderer(new DisplayModeRenderer());
		displayModeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		displayModeList.setLayoutOrientation(JList.VERTICAL);
		displayModeList.setVisibleRowCount(10);
	}

	private Vector<DisplayMode> filterDisplayModes(DisplayMode[] displayModes, int minWidth, int minHeight) {
		Vector<DisplayMode> filteredModes = new Vector<DisplayMode>(displayModes.length);
		for (DisplayMode displayMode : displayModes) {
			if (displayMode.getWidth() >= minWidth && displayMode.getHeight() >= minHeight) {
				filteredModes.add(displayMode);
			}
		}
		return filteredModes;
	}

	public VideoConfigData getVideoConfigData() {
		VideoConfigData configData = new VideoConfigData(displayModeList.getSelectedValue(), false, debugMode.isSelected());
		return configData;

	}

	private void createRunPanel() {
		JPanel runPanel = new JPanel();
		contentPane.add(runPanel, BorderLayout.SOUTH);
		JButton btnNewButton = new JButton("Run!");
		btnNewButton.addActionListener(runActionListener);
		runPanel.add(btnNewButton);
	}
}
