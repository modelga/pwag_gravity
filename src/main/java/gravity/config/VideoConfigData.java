package gravity.config;

import java.awt.DisplayMode;

public class VideoConfigData {
	private final DisplayMode displayMode;
	private final boolean debugMode;

	public VideoConfigData(DisplayMode displayMode, boolean fullscreen, boolean debugMode) {
		this.displayMode = displayMode;
		this.debugMode = debugMode;
	}

	public DisplayMode getDisplayMode() {
		return displayMode;
	}

	public int getBitDepth() {
		return displayMode.getBitDepth();
	}

	public int getHeight() {
		return displayMode.getHeight();
	}

	public int getRefreshRate() {
		return displayMode.getRefreshRate();
	}

	public int getWidth() {
		return displayMode.getWidth();
	}

	public boolean isDebugMode() {
		return debugMode;
	}
}