package gravity.config;

import java.awt.Component;
import java.awt.DisplayMode;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

public class DisplayModeRenderer extends DefaultListCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8426316625954140771L;

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		if (index != -1) {
			if (value instanceof DisplayMode) {
				DisplayMode mode = (DisplayMode) value;
				value = getModeLabel(mode);
			}
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}

	private String getModeLabel(DisplayMode mode) {
		return mode.getWidth() + "x" + mode.getHeight() + " " + mode.getBitDepth() + " bit (" + mode.getRefreshRate() + "Hz)";
	}
}
