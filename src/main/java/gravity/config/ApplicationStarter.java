package gravity.config;

import gravity.main.Application;
import gravity.main.DebugApplication;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

final class ApplicationStarter implements ActionListener {
	private final VideoConfigWindow videoConfigWindow;

	ApplicationStarter(VideoConfigWindow videoConfigWindow) {
		this.videoConfigWindow = videoConfigWindow;
	}

	public void actionPerformed(ActionEvent arg0) {

		VideoConfigData videoConfigData = videoConfigWindow.getVideoConfigData();
		Object application = runProperApplication(videoConfigData);
		System.out.println("Running " + application.getClass().getSimpleName() + "  in " + videoConfigData.getWidth() + " x " + videoConfigData.getHeight());

	}

	private Object runProperApplication(VideoConfigData videoConfigData) {
		Object application;
		if (!videoConfigData.isDebugMode()) {
			application = new Application(videoConfigData);
			this.videoConfigWindow.setVisible(false);
		} else {
			application = new DebugApplication(videoConfigData);
		}
		return application;
	}
}