package gravity.conector;

import gravity.physics.DynamicCelestialBody;
import gravity.physics.util.Position;
import gravity.system.api.CelestialBodyLifeCycleListener;
import gravity.system.manager.CelestialSystem;

import java.util.ArrayList;
import java.util.Collection;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import static gravity.conector.VisualPhysicsConnector.multiplySIMeterToDouble;

public class VisualPhysicsArray implements CelestialBodyLifeCycleListener {
	public ArrayList<VisualPhysicsConnector> vpList;

	String newLine = System.getProperty("line.separator");

	public VisualPhysicsArray(CelestialSystem system) {
		vpList = new ArrayList<VisualPhysicsConnector>();
	}

	private void putToBuffer(VisualPhysicsConnector vpObject) {
		vpList.add(vpObject);
	}

	public Array<ModelInstance> getInstances() {
		Array<ModelInstance> array = new Array<ModelInstance>();
		for (VisualPhysicsConnector connector : getVisualPhysicsConnectors()) {
			array.add(connector.getVisualBody());
		}
		return array;
	}

	public Array<DynamicCelestialBody> getDynamicCelestialBodyArray() {
		Array<DynamicCelestialBody> array = new Array<DynamicCelestialBody>();
		for (VisualPhysicsConnector connector : getVisualPhysicsConnectors()) {
			array.add(connector.getDynamicBody());
		}
		return array;
	}

	public ArrayList<VisualPhysicsConnector> getVisualPhysicsConnectors() {
		return vpList;
	}

	public float getRadiusOfSphere(DynamicCelestialBody body) {
		float radius = (float) (body.getBody().getRadius().getEstimatedValue() / multiplySIMeterToDouble);
		return radius;
	}

	public String getNameOfSphere(DynamicCelestialBody body) {
		String primName = body.getBody().getName();
		return primName;
	}

	public Vector3 getPosition(DynamicCelestialBody body) {
		Position absolutePosition = body.getAbsolutePosition();
		float x = (float) (absolutePosition.getX().getEstimatedValue() / multiplySIMeterToDouble);
		float y = (float) (absolutePosition.getY().getEstimatedValue() / multiplySIMeterToDouble);
		float z = (float) (absolutePosition.getZ().getEstimatedValue() / multiplySIMeterToDouble);
		Vector3 result = new Vector3(x, y, z);
		return result;
	}

	@Override
	public void addBody(DynamicCelestialBody body) {
		putToBuffer(new VisualPhysicsConnector(body));
	}

	@Override
	public void addBody(Collection<DynamicCelestialBody> bodies) {
		for (DynamicCelestialBody body : bodies) {
			putToBuffer(new VisualPhysicsConnector(body));
		}
	}

	@Override
	public void removeBody(DynamicCelestialBody body) {
		throw new UnsupportedOperationException("Remove single body is not supported");
	}

	@Override
	public void removeAll() {
		vpList.clear();
	}
}