package gravity.conector;

import gravity.physics.DynamicCelestialBody;
import gravity.physics.util.Position;
import gravity.visual.utils.TextureBuilder;

import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

public class VisualPhysicsConnector {
	public static final double multiplySIMeterToDouble = 10000000.0;
	private final DynamicCelestialBody dynamicBody;
	private final ModelInstance visualBody;

	public VisualPhysicsConnector(DynamicCelestialBody dynaBody) {
		this.dynamicBody = dynaBody;
		ModelBuilder modelBuilder = new ModelBuilder();

		Material material;
		if (this.dynamicBody.isSun()) {
			material = new Material(new TextureBuilder().getSunTexture());
		} else {
			material = new Material(new TextureBuilder().getPlanetTexture());
		}

		Model model = modelBuilder.createSphere((float) (dynaBody.getBody().getRadius().getEstimatedValue() * 2 / multiplySIMeterToDouble), (float) (dynaBody
				.getBody().getRadius().getEstimatedValue() * 2 / multiplySIMeterToDouble),
				(float) (dynaBody.getBody().getRadius().getEstimatedValue() * 2 / multiplySIMeterToDouble), 32, 32, material, Usage.Position | Usage.Normal
						| Usage.TextureCoordinates);
		this.visualBody = new ModelInstance(model);
		this.visualBody.transform.setToTranslation((float) (dynaBody.getAbsolutePosition().getX().getEstimatedValue() / multiplySIMeterToDouble),
				(float) (dynaBody.getAbsolutePosition().getY().getEstimatedValue() / multiplySIMeterToDouble), (float) (dynaBody.getAbsolutePosition().getZ()
						.getEstimatedValue() / multiplySIMeterToDouble));
		// System.out.println((float)
		// (dynaBody.getBody().getRadius().getEstimatedValue() /
		// multiplySIMeterToDouble));
	}

	public DynamicCelestialBody getDynamicBody() {
		return dynamicBody;
	}

	public ModelInstance getVisualBody() {
		return visualBody;
	}

	public static double[] translatePosition(Position position) {
		return new double[] { position.getX().getEstimatedValue() / multiplySIMeterToDouble, position.getY().getEstimatedValue() / multiplySIMeterToDouble,
				position.getZ().getEstimatedValue() / multiplySIMeterToDouble };
	}
}
